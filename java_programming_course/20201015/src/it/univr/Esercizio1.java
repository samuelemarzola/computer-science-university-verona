/*
 * Si scriva, usando Eclipse, un programma il cui main legge un intero 
 * non negativo da tastiera (se negativo, lo richiede ad oltranza) e lo trasforma in una 
 * stringa che rappresenta lo stesso numero in binario, che infine stampa. 
 * Per esempio, se viene immesso l'intero 123, 
 * deve costruire e poi stampare la stringa "1111011".
 */

package it.univr;

import java.util.Scanner;

public class Esercizio1 {
	
	public static void main(String[] args) {
		int n; 
		String s = "";
		Scanner keyboard = new Scanner(System.in);
		
		do {
			System.out.print("Inserire un n non negativo: ");
			n = keyboard.nextInt();
		}
		while (n < 0);
		
		while (n > 0) {
			s = n%2 + s;
			n /= 2;
		}
		
		System.out.print(s);
		
		keyboard.close();
	}

}
