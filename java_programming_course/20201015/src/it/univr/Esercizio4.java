/*
 * Si scriva un programma Java che legge una frase da tastiera e determina 
 * se tale frase è palindroma.
 */

package it.univr;

import java.util.Scanner;

public class Esercizio4 {
	
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		String text;
		int check = 0;
		
		System.out.print("Frase: ");
		text = keyboard.nextLine();
		
		for (int i = 1; i < text.length(); i++) {
			if (!(text.charAt(i - 1) == text.charAt(text.length() - i))) {
				check++;
			}
		}
		
		if (check == 0) {
			System.out.println("La frase è palindroma! ");
		}
		else {
			System.out.println("La frase NON è palindroma! ");
		}
		
		keyboard.close();
	}

}
