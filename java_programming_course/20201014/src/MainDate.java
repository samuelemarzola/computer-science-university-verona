package it.univr;

public class MainDate {

	public static void main(String[] args) {
		Date d1 = new Date(7, 10, 2020);
		Date d2 = new Date(13, 1, 1973);
		System.out.println(d1);
		System.out.println(d2);
		Date.setNationality(Nationality.AMERICAN);
		System.out.println(d1);
		System.out.println(d2);
		Date.setNationality(Nationality.ITALIAN);
		System.out.println(d1);
		System.out.println(d2);
		//Date.setNationality(-13); // non bello
	}
}