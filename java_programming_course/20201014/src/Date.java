package it.univr;

public class Date {
	// campi (variabili di ciascun oggetto)
	// formano lo stato degli oggetti di classe Date
	// li definiamo private per garantire l'incapsulazione
	private final int day;   // si può usare questo campo solo dentro Date.java
	private final int month;
	private final int year;

	/**
	 * 0 significa italiano
	 * 1 significa americano
	 */
	private static Nationality nationality = Nationality.ITALIAN;

	// costruttore: si DEVE chiamare come la classe
	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}

	// metodo degli oggetti della classe
	public String toString() {
		switch (nationality) {
		case ITALIAN: return day + "/" + month + "/" + year;
		case GERMAN: return year + "/" + month + "/" + day;
		case AMERICAN: return month + "/" + day + "/" + year;
		default: return "nazionality illegale";
		}
	}

	/**
	 * 0 significa italiano
	 * 1 significa americano
	 */
	public static void setNationality(Nationality nationality) {
		Date.nationality = nationality;
	}
	
	// other è un parametro formale di equals()
	// il parametro formale è una dichiarazione di variabile
	public boolean equals(Date other) {
		// ci sono due Date qui dentro: this e other
		return day == other.day && month == other.month && year == other.year;
	}

	// convenzione:
	// ritorna un numero negativo se this viene prima di other
	// ritorna un numero positivo se other viene prima di this
	// ritorna 0 se this e other sono semanticamente uguali
	public int compareTo(Date other) {
		// questa versione è corretta ipotizzando che la differenza
		// dei day, month, year non vada in overflow
		int diff = year - other.year;
		if (diff != 0)
			return diff;

		// se month fosse il massimo int esistente e se other.month fosse 1
		diff = month - other.month;
		if (diff != 0)
			return diff;

		return day - other.day;
	}

	/*public int compareTo(Date other) {
		// ci sono due Date qui dentro: this e other
		if (year < other.year)
			return -1; // prima this
		else if (year > other.year)
			return 1; // prima other
		else if (month < other.month)
			return -1; // prima this
		else if (month > other.month)
			return 1; // prima other
		else if (day < other.day)
			return -1; // prima this
		else if (day > other.day)
			return 1; // prima other
		else
			return 0; // semanticamente uguali
	}*/
	
}