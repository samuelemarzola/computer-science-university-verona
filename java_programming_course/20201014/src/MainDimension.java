package it.univr;

public class MainDimension {
	public static void main(String[] args) {
		Dimension d1 = Dimension.SMALL;
		Dimension d2 = Dimension.HUGE;
		
		if (d1.compareTo(d2) < 0)
			System.out.println(d1 + " e' piu' piccolo di " + d2);
		else if (d1.compareTo(d2) > 0)
			System.out.println(d1 + " e' piu' grande di " + d2);
		else
			System.out.println(d1 + " e' uguale a " + d2);

		System.out.println(d1 + ".ordinal() = " + d1.ordinal());
		System.out.println(d2 + ".ordinal() = " + d2.ordinal());
		
		Dimension[] elements = Dimension.values();
		for (int pos = 0; pos < 5; pos++)
			System.out.println("elements[" + pos + "] = " + elements[pos]);

		Dimension m = Dimension.valueOf("MEDIUM");
		// equivale in questo caso a Dimension m = Dimension.MEDIUM;
		System.out.println("m = " + m);
	}
}
