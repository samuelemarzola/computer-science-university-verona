package it.univr.notes;

public interface NoteWithDuration {
	public Duration getDuration();
}