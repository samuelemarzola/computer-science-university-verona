package it.univr;

import java.util.Scanner;

public class CircleArea {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Raggio: ");
		double radius = keyboard.nextDouble();
		double area = radius * radius * Math.PI;
		String s = String.format("L'area del cerchio di raggio %.4f è %.4f", radius, area);
		System.out.println(s);
		keyboard.close();
	}
}
