package it.univr;

import java.util.Random;
import java.util.Scanner;

/**
 * Genera e stampa una stringa a casa di caratteri alfabetici minuscoli (a-z).
 */
public class RandomString {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Quanto deve essere lunga la stringa? ");
		int length = keyboard.nextInt();
		
		Random random = new Random();
		String s = "";

		while (length-- > 0) {
			// genero un carattere a caso c
			int index = random.nextInt(26);
			char c = (char) ('a' + index);
			s += c; // concatenazione fra stringa e carattere
		}

		System.out.println(s);

		keyboard.close();
	}
}