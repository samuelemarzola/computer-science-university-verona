package it.univr;

import java.util.Random;
import java.util.Scanner;

/**
 * Lancia un dado tante volte, in maniera casuale.
 */
public class RandomDice {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Quanti lanci di due dadi devo fare? ");
		int quanti = keyboard.nextInt();
		
		Random random = new Random();

		while (quanti-- > 0) {
			// devo generare un numero tra 1 e 6 inclusi
			int dado1 = 1 + random.nextInt(6);
			int dado2 = 1 + random.nextInt(6);
			// in genere se voglio un numero casuale da x ad y (inclusi)
			// x + random.nextInt(y - x + 1)
			
			System.out.println(dado1 + dado2);
		}
		
		keyboard.close();
	}
}