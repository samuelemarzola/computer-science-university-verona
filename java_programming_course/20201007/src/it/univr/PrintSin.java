package it.univr;

public class PrintSin {

	public static void main(String[] args) {
		for (double x = 0.0; x < 1000.0; x += 0.1) {
			double l = Math.sin(x); // l va da -1 a 1
			l++; // adesso l va da 0 a 2
			l /= 2; // adesso l va da 0 a 1
			
			// voglio stampare l * 40 asterischi
			for (int i = 0; i < l * 40; i++)
				System.out.print('*');

			System.out.println();
		}
	}
}
