package it.univr;

public class MainDate {

	public static void main(String[] args) {
		Date d1 = new Date(7, 10, 2020);
		Date d2 = new Date(13, 1, 1973);
		System.out.println(d1 == d2);
		System.out.println(d1.toString());
		System.out.println(d2.toString());
	}
}