package it.univr;

import java.util.Random;
import java.util.Scanner;

public class RandomNumbers {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Quanti numeri casuali vuoi? ");
		int quanti = keyboard.nextInt();
		
		Random random = new Random();

		while (quanti-- > 0) {
			int i = random.nextInt();
			System.out.println(i);
		}
		
		keyboard.close();
	}
}