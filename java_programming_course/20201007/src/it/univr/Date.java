package it.univr;

public class Date {
	// campi (variabili di ciascun oggetto)
	private int day;
	private int month;
	private int year;

	// costrutore: si DEVE chiamare come la classe
	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}

	// metodo degli oggetti della classe
	public String toString() {
		return day + "/" + month + "/" + year;
	}
}
