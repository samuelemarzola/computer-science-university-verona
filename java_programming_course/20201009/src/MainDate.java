package it.univr;

public class MainDate {

	public static void main(String[] args) {
		Date d1 = new Date(7, 10, 2020);
		Date d2 = new Date(13, 1, 1973);
		System.out.println(d1 == d2);
		System.out.println(d1); // qui e' sottinteso d1.toString()
		System.out.println("d1 = " + d1); // qui e' sottinteso d1.toString()
		System.out.println("d2 = " + d2.toString());
		System.out.println("d1 = " + d1); // qui e' sottinteso d1.toString()
		Date d3 = new Date(13, 1, 1973);
		System.out.println("d2 è uguale a d3? " + (d2 == d3));
		// sotto, d3 è un parametro attuale passato ad equals
		// il parametro attuale è il valore di un'espressione 
		System.out.println("d2 è uguale a d3? " + d2.equals(d3));
		System.out.println("d2 viene prima di d3? " + d2.compareTo(d3));
		System.out.println("d1 viene prima di d3? " + d1.compareTo(d3));
		System.out.println("d3 viene prima di d1? " + d3.compareTo(d1));
		if (d3.compareTo(d1) < 0) {
			// d3 viene prima di d1
		}
	}
}