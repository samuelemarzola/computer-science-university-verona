/* Si scriva un programma che legge da tastiera un long non negativo n e lo richiede a oltranza 
 * se l'utente lo inserisse negativo. 
 * Quindi genera n coppie (x,y) fatte da due numeri casuali di tipo double, fra -1 e 1. 
 * Per ogni coppia controlla se la coordinata (x,y) sta dentro il cerchio di raggio 1
 * centrato sull'origine degli assi e in tal caso incrementa una variabile dentro di tipo long. 
 * Alla fine stampa il valore della formula dentro * 4 / n senza perdere le cifre che seguono la virgola. */


package it.univr;

import java.util.Random;
import java.util.Scanner;

public class Dentro {
	
	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		Random random = new Random();
		
		long n, dentro = 0, diametro, raggio = 1;
		double x, y, result = 0;

		diametro = 2 * raggio;
		
		do {
			System.out.print("Inserire un numero non negativo: ");
			n = keyboard.nextLong();
		}
		while (n < 0);
		
		keyboard.close();
		
		for (int i = 0; i < n; i++) {
			x = 1 - random.nextDouble() * 2 - 1;
			y = 1 - random.nextDouble() * 2 - 1;
			result = Math.sqrt((x * x) + (y * y));
			
			if (result <= diametro / 2) {
				dentro++;
			}
		}
		
		System.out.println("Risultato " + dentro * 4.0 / n);
	}
}
