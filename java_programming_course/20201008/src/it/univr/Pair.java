package it.univr;

public class Pair {
	
	private long x;
	private long y;
	
	public Pair(long x, long y) {
		this.x = x;
		this.y = y;
	}
	
	public String toString() {
		return "<" + x + ", " + y + ">";
	}

}
