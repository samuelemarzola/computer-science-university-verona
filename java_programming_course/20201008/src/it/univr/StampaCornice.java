package it.univr;

import java.util.Scanner;

public class StampaCornice {

	public static void main(String[] args) {
		int n;
		
		Scanner keyboard = new Scanner(System.in);

		do {
			System.out.print("Inserisci n non negativo: ");
			n = keyboard.nextInt();
		}
		while (n < 0);
		
		keyboard.close();

		String top;
		String middle;

		for (top = ""; top.length() < n; top += '@');
		for (middle = "@"; middle.length() < n - 1; middle += ' ');
		middle += '@';
		
		String risultato;
		risultato = top;
		
		for (int i = 0; i < n - 2; i++)
			risultato += "\n" + middle;
		
		if (n > 1)
			risultato += "\n" + top;
		else
			risultato += "\n";

		System.out.println(risultato);
	}
}