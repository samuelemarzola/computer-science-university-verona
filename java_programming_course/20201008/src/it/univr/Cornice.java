/* Si scriva un programma Java che legge un intero non negativo n da tastiera e stampa una cornice n x n:
@@@@@@
@    @
@    @
@    @
@    @
@@@@@@
*/


package it.univr;

import java.util.Scanner;

public class Cornice {
	public static void main(String[] args) {
		int n; 
		
		Scanner keyboard = new Scanner(System.in);
		
		do {
			System.out.print("Inserire un n non negativo: ");
			n = keyboard.nextInt();
		}
		while (n < 0); 
		
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if ( (j > 0) && (j < n - 1) && (i > 0) && (i < n - 1) ) {
					System.out.print(" ");
				}
				else {
					System.out.print("@");
				}
			}
			System.out.print("\n");
		}
		
		keyboard.close();
	}
}
