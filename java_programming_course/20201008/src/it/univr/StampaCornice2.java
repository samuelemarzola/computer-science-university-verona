package it.univr;

public class StampaCornice2 {
	
	private int x;
	
	public StampaCornice2(int x) {
		this.x = x;
	}
	
	public void print() {
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < x; j++) {
				if (i > 0 && i < x - 1 && j > 0 && j < x - 1) {
					System.out.print(" ");
				}
				else {
					System.out.print("@");
				}
			}
			System.out.print("\n");
		}
	}

}
