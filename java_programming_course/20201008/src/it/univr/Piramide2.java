/* Si scriva un programma che legge n >= 1 da tastiera e stampa una piramide di altezza n. 
 * Per esempio, per n = 4 deve stampare:
   @
  @@
 @@@
@@@@
 @@@
  @@
   @
 * 
 */
package it.univr;

import java.util.Scanner;

public class Piramide2 {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in); 
		
		int n, i, j, temp;
		
		do {
			System.out.print("Inserire un n maggiore o uguale a 1: ");
			n = keyboard.nextInt();
		}
		while (n < 1);
		
		temp = n;
		
		for (i = 0; i < (n * 2); i++) {
			
			if (i < n) {
				temp--;
			}
			else {
				temp++;
			}
			
			for (j = 0; j < n; j++) {
				
				if ((j >= temp) && (i <= n) || (i > n) && (j >= temp)) {
					System.out.print("@");				
				}
				else {
					System.out.print(" ");
				}
				
			}
			
			System.out.print("\n");
			
		}
		
		keyboard.close();
		
	}
}
