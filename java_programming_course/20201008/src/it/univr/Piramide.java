/* Si scriva un programma che legge n >= 1 da tastiera e stampa una piramide di altezza n. 
 * Per esempio, per n = 4 deve stampare:
   @
  @@@
 @@@@@
@@@@@@@
 * 
 */
package it.univr;

import java.util.Scanner;

public class Piramide {
	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		int n;
		
		do {
			System.out.println("Inserire un n maggiore o uguale a 1");
			n = keyboard.nextInt();
		}
		while (n < 1);
		
		int temp = n - 1;
		
		for (int i = 0; i <= n; i++, temp--) {
			for (int j = 0; j <= temp; j++) {
				if (j < temp)
					System.out.print(" ");
				else {
					for (int z = 0; z <= (i + i); z++)
					System.out.print("@");
				}
			}
			System.out.print("\n");
		}
		
		keyboard.close();
	}
}
