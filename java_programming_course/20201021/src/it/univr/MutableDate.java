package it.univr;

import java.util.Random;

// questa e' una classe mutabile
// cioe' i suoi oggetti sono mutabili
public class MutableDate {
	// campi (variabili di ciascun oggetto)
	// formano lo stato degli oggetti di classe Date
	// li definiamo private per garantire l'incapsulazione
	private int day;   // si può usare questo campo solo dentro Date.java
	private int month;
	private int year;

	/**
	 * 0 significa italiano
	 * 1 significa americano
	 */
	private static Nationality nationality = Nationality.ITALIAN;

	// costruttore: si DEVE chiamare come la classe
	public MutableDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}

	// costruttore: costruisce il primo giorno del mese indicato
	public MutableDate(int month, int year) {
		this(1, month, year); // delega al primo costruttore
	}

	// costruttore: costruisce il primo gennaio dell'anno indicato
	public MutableDate(int year) {
		//this(1, 1, year);
		this(1, year);
	}

	// costruttore: costruisce una data a caso tra il 2000 e il 2100
	public MutableDate() {
		Random random = new Random();
		this.month = random.nextInt(12) + 1;
		this.year = random.nextInt(101) + 2000;
		int dim = daysInMonth(month, year);
		this.day = random.nextInt(dim) + 1;
	}

	// metodo di mutazione
	public void increase() {
		if (++day > daysInMonth(month, year)) {
			day = 1;
			if (++month == 13) {
				month = 1;
				year++;
			}
		}
	}

	private static final int[] DAYS = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	private int daysInMonth(int m, int y) {
		if (m == 2 && isLeapYear(y))
			return 29;

		return DAYS[m - 1];
	}

	private boolean isLeapYear(int y) {
		return y % 4 == 0 && (y % 100 != 0 || y % 400 == 0);
	}

	public Season getSeason() {
		MutableDate springStart = new MutableDate(21, 3, year);
		MutableDate summerStart = new MutableDate(21, 6, year);
		MutableDate autumnStart = new MutableDate(23, 9, year);
		MutableDate winterStart = new MutableDate(21, 12, year);
		
		if (this.compareTo(springStart) >= 0 && this.compareTo(summerStart) < 0)
			return Season.SPRING;
		
		if (this.compareTo(summerStart) >= 0 && this.compareTo(autumnStart) < 0)
			return Season.SUMMER;

		if (this.compareTo(autumnStart) >= 0 && this.compareTo(winterStart) < 0)
			return Season.AUTUMN;

		return Season.WINTER;
	}

	// metodo degli oggetti della classe
	public String toString() {
		switch (nationality) {
		case ITALIAN: return day + "/" + month + "/" + year;
		case GERMAN: return year + "/" + month + "/" + day;
		case AMERICAN: return month + "/" + day + "/" + year;
		default: return "nazionality illegale";
		}
	}

	/**
	 * 0 significa italiano
	 * 1 significa americano
	 */
	public static void setNationality(Nationality nationality) {
		MutableDate.nationality = nationality;
	}
	
	// other è un parametro formale di equals()
	// il parametro formale è una dichiarazione di variabile
	public boolean equals(MutableDate other) {
		// ci sono due Date qui dentro: this e other
		return day == other.day && month == other.month && year == other.year;
	}

	// convenzione:
	// ritorna un numero negativo se this viene prima di other
	// ritorna un numero positivo se other viene prima di this
	// ritorna 0 se this e other sono semanticamente uguali
	public int compareTo(MutableDate other) {
		// questa versione è corretta ipotizzando che la differenza
		// dei day, month, year non vada in overflow
		int diff = year - other.year;
		if (diff != 0)
			return diff;

		// se month fosse il massimo int esistente e se other.month fosse 1
		diff = month - other.month;
		if (diff != 0)
			return diff;

		return day - other.day;
	}

	/*public int compareTo(Date other) {
		// ci sono due Date qui dentro: this e other
		if (year < other.year)
			return -1; // prima this
		else if (year > other.year)
			return 1; // prima other
		else if (month < other.month)
			return -1; // prima this
		else if (month > other.month)
			return 1; // prima other
		else if (day < other.day)
			return -1; // prima this
		else if (day > other.day)
			return 1; // prima other
		else
			return 0; // semanticamente uguali
	}*/
	
}