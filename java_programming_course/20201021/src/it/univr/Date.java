package it.univr;

import java.util.Random;

/**
 * Questa e' una classe immutabile che rappresenta una data del calendario.
 * Quindi i suoi oggetti sono immutabili.
 * 
 * @author Fausto Spoto
 */
public class Date {
	
	/**
	 * Il giorno della data.
	 */
	private final int day;

	/**
	 * Il mese della data.
	 */
	private final int month;

	/**
	 * L'anno della data.
	 */
	private final int year;

	/**
	 * La nazionalita' delle date.
	 */
	private static Nationality nationality = Nationality.ITALIAN;

	/**
	 * Costruisce una data arbitraria.
	 * 
	 * @param day il giorno della data
	 * @param month il mese della data
	 * @param year l'anno della data
	 */
	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}

	/**
	 * Costruisce il primo giorno del mese indicato.
	 * 
	 * @param month il mese della data
	 * @param year l'anno della data
	 */
	public Date(int month, int year) {
		this(1, month, year); // delega al primo costruttore
	}

	/**
	 * Costruisce il primo gennaio dell'anno indicato.
	 * 
	 * @param year l'anno della data
	 */
	public Date(int year) {
		//this(1, 1, year);
		this(1, year);
	}

	/**
	 * Costruisce una data a caso tra il 2000 e il 2100.
	 */
	public Date() {
		Random random = new Random();
		this.month = random.nextInt(12) + 1;
		this.year = random.nextInt(101) + 2000;
		int dim = daysInMonth(month, year);
		this.day = random.nextInt(dim) + 1;
	}

	private static final int[] DAYS = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	private int daysInMonth(int m, int y) {
		if (m == 2 && isLeapYear(y))
			return 29;

		return DAYS[m - 1];
	}

	private boolean isLeapYear(int y) {
		return y % 4 == 0 && (y % 100 != 0 || y % 400 == 0);
	}

	/**
	 * Restituisce la stagione di questa data.
	 * 
	 * @return la stagione di questa data
	 */
	public Season getSeason() {
		Date springStart = new Date(21, 3, year);
		Date summerStart = new Date(21, 6, year);
		Date autumnStart = new Date(23, 9, year);
		Date winterStart = new Date(21, 12, year);
		
		if (this.compareTo(springStart) >= 0 && this.compareTo(summerStart) < 0)
			return Season.SPRING;
		
		if (this.compareTo(summerStart) >= 0 && this.compareTo(autumnStart) < 0)
			return Season.SUMMER;

		if (this.compareTo(autumnStart) >= 0 && this.compareTo(winterStart) < 0)
			return Season.AUTUMN;

		return Season.WINTER;
	}

	/**
	 * Restituisce una descrizione di questa data.
	 * 
	 * @return la descrizione, come stringa
	 */
	public String toString() {
		switch (nationality) {
		case ITALIAN: return day + "/" + month + "/" + year;
		case GERMAN: return year + "/" + month + "/" + day;
		case AMERICAN: return month + "/" + day + "/" + year;
		default: return "nazionality illegale";
		}
	}

	/**
	 * Setta la nazionalita' delle date.
	 * 
	 * @param nationality la nazionalita' da settare
	 */
	public static void setNationality(Nationality nationality) {
		Date.nationality = nationality;
	}
	
	public boolean equals(Date other) {
		// ci sono due Date qui dentro: this e other
		return day == other.day && month == other.month && year == other.year;
	}

	/**
	 * Confronta questa data con un'altra data.
	 * 
	 * @param other l'altra data
	 * @return negativo, se this viene prima di other; positivo, se this viene
	 *         dopo di other, 0 se this e other sono equivalenti
	 */
	public int compareTo(Date other) {
		// questa versione è corretta ipotizzando che la differenza
		// dei day, month, year non vada in overflow
		int diff = year - other.year;
		if (diff != 0)
			return diff;

		// se month fosse il massimo int esistente e se other.month fosse 1
		diff = month - other.month;
		if (diff != 0)
			return diff;

		return day - other.day;
	}

	/*public int compareTo(Date other) {
		// ci sono due Date qui dentro: this e other
		if (year < other.year)
			return -1; // prima this
		else if (year > other.year)
			return 1; // prima other
		else if (month < other.month)
			return -1; // prima this
		else if (month > other.month)
			return 1; // prima other
		else if (day < other.day)
			return -1; // prima this
		else if (day > other.day)
			return 1; // prima other
		else
			return 0; // semanticamente uguali
	}*/
	
}