package it.univr;

public class MainDate {

	public static void main(String[] args) {
		Date d1 = new Date(21, 10, 2020); // chiama il primo costruttore
		Date copy = d1;
		Date d2 = new Date(11, 2020); // chiama il secondo costruttore
		Date d3 = new Date(2020); // chiama il terzo costruttore
		System.out.println(d1);
		System.out.println(d2);
		System.out.println(d3);

		for (int counter = 1; counter <= 100; counter++)
			System.out.println(new Date()); // chiama il quarto costruttore

		// rendo d1 il 22/10/2020
		d1 = new Date(22, 10, 2020); // riassegna la variabile, non modifica l'oggetto
		System.out.println("d1 = " + d1);
		System.out.println("copy = " + copy);
	}
}
