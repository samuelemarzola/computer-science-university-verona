package it.univr;

public class TestArrayAssign {
	public static void main(String[] args) {
		int[] arr1; // solo dichiarazione, nessuna creazione
		arr1 = new int[5]; // gli elementi valgono 0
		int[] arr2 = { 2, 3, 7, -2, 2, 8, 12 };
		
		arr1 = arr2;
		for (int element: arr1)
			System.out.println(element);

		arr2[3] = 13;

		for (int element: arr1)
			System.out.println(element);
		
		arr1 = new int[3];
		for (int element: arr1)
			System.out.println(element);
	}
}