package it.univr;

import java.util.Arrays;

public class MainDate {

	public static void main(String[] args) {
		Date d1 = new Date(7, 10, 2020);
		Date d2 = new Date(13, 1, 1973);
		
		Date[] arr = new Date[5]; // elementi a null
		arr[0] = d1;
		arr[1] = d2;
		arr[2] = null;
		arr[3] = d2;
		arr[4] = new Date(6, 2, 1980);
		
		Date[] arr2 = { d1, d2, null, d2, new Date(6, 2, 1980) };
		for (Date element: arr)
			System.out.println(element);
		
		System.out.println(arr); // stampa il puntatore
		System.out.println(Arrays.toString(arr)); // stampa separando con virgole
		
		Date[] result = removeNull(arr2);
		System.out.println(Arrays.toString(result));
	}

	public static Date[] removeNull(Date[] par) {
		// calcolo quanti elementi sono diversi da null
		int l = 0;
		for (Date element: par)
			if (element != null)
				l++;

		int pos = 0;
		Date[] res = new Date[l];
		for (Date element: par)
			if (element != null)
				res[pos++] = element;

		return res;
	}
}