package it.univr;

import java.util.Arrays;

public class MainDate2 {

	public static void main(String[] args) {
		Date d1 = new Date(7, 10, 2020);
		Date d2 = new Date(13, 1, 1973);
		
		Date[] arr = new Date[8]; // elementi a null
		arr[0] = d1;
		arr[1] = d2;
		arr[2] = new Date(20, 8, 1999);
		arr[3] = new Date(11, 12, 2008);
		arr[4] = new Date(6, 2, 1980);
		arr[5] = new Date(17, 7, 2020);
		arr[6] = new Date(13, 5, 2018);
		arr[7] = new Date(28, 2, 2011);
		
		for (Date d: arr)
			System.out.println(d + ": " + d.getSeason());
		
		// voglio contare quante date ci sono per ciascuna stagione
		int[] counters = new int[4]; // inizializzati a 0
		
		// se voglio essere indipendente dal numero di elementi nella enum Season
		//int[] counters = new int[Season.values().length]; // inizializzati a 0

		// stampo tutte le Season
		System.out.println(Arrays.toString(Season.values()));

		/*for (Date d: arr) {
			if (d.getSeason() == Season.WINTER)
				counters[0]++;
			else if (d.getSeason() == Season.SPRING)
				counters[1]++;
			else if (d.getSeason() == Season.SUMMER)
				counters[2]++;
			else
				counters[3]++;
		}*/
		for (Date d: arr)
			counters[d.getSeason().ordinal()]++;

		System.out.println(Arrays.toString(counters));

		// qual è la stagione in cui ricadono esattamente 2 date?
		for (int pos = 0; pos < counters.length; pos++)
			if (counters[pos] == 2)
				System.out.println("Una stagione con due date e': " + Season.values()[pos]);

		Arrays.sort(counters);
		//Arrays.parallelSort(counters);

		System.out.println(Arrays.toString(counters));
	}
}