package it.univr;

import java.util.Scanner;

public class Repeat {

	public static void main(String[] args) {
	
		String frase; 
		
		Scanner keyboard = new Scanner(System.in);
		
		do {
			System.out.print("Inserisci una frase: ");
			frase = keyboard.nextLine();
		}
		// while (frase != "exit"); ERROR
		while (!frase.contentEquals("exit"));
		
		keyboard.close();
	}
	
}