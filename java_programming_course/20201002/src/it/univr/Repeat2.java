package it.univr;

import java.util.Scanner;

public class Repeat2 {

	public static void main(String[] args) {
		String frase;
		Scanner keyboard = new Scanner(System.in);

		do {
			System.out.print("Inserisci una frase: ");
			frase = keyboard.nextLine();
			
			System.out.println("Gli ultimi tre caratteri inseriti sono "
				+ frase.substring(frase.length() - 3));
			System.out.println("Il carattere i si trova in posizione " + frase.indexOf('i'));

			System.out.println(frase.compareTo("ciao"));
			// int esito = a.compareTo(b);
			// esito < 0 se a viene prima di b
			// esito > 0 se a viene dopo di b
			// esito == 0 se a e b sono equivalenti

			System.out.println(frase + "!!!!");
			//System.out.println(frase.concat(String.valueOf(13)));
			System.out.println(frase + 13);
			System.out.println
				("La tua frase e' lunga " + frase.length() + " il primo carattere e' " + frase.charAt(0));
			System.out.println
				(String.format("La tua frase e' lunga %d il primo carattere e' %c\n",
					frase.length(), frase.charAt(0)));

			// sostituiamo il quarto carattere (in posizione 3) della stringa con una 'z'
			frase = frase.substring(0, 3) + 'z' + frase.substring(4);
			System.out.println(frase);

			long millis = System.currentTimeMillis();
			System.out.println("millis = " + millis);
			
			for (int pos = 0; pos < frase.length(); pos++)
				System.out.println(pos + ": " + frase.charAt(pos));
		}
		while (frase.length() <= 2 || frase.charAt(2) != 'i'); // cicla se il terzo carattere non e' una 'i'

		keyboard.close();
	}
}
