package it.univr;

import java.util.Scanner;

public class Input {

	public static void main(String[] args) {
		System.out.print("Frase da ripetere: ");
		String frase;

		Scanner keyboard = new Scanner(System.in);
		frase = keyboard.nextLine();

		System.out.println(frase);

		keyboard.close();
	}
}