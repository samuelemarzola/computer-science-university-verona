package it.univr;

public class Vars {

	public static void main(String[] args) {
		// tipi primitivi: non sono oggetti
		// sono tutti scritti minuscoli
		// boolean, byte, char, short, int, long || float, double

		int i;
		i = 13;
		System.out.println("i = " + i);

		long l = 1345678466;
		System.out.println("l = " + l);

		float f = 3.14f;
		System.out.println("f = " + f);

		final double d = 3.14;
		System.out.println("d = " + d);

		final boolean b = true;
		System.out.println("b = " + b);

		l = i;
		System.out.println("l = " + l);

		// i = l; // non compila perché int = long
		i = (int) l;
		System.out.println("i = " + i);

		char c = 'a';
		System.out.println("c = " + c);

		// assegnamento: leftvalue = rightvalue
		// rightvalue è una qualsiasi espressione
		// leftvalue ammette solo alcune espressioni:
		// 1) variabile: v = 13
		// 2) elemento di array v[3] = 17
		// 3) campo di oggetto: v.f = 13

		i += 5; // i = i + 5;
		i++; // i = i + 1; postincremento
		++i; // i = i + 1; preincremento

		int k = 5;
		System.out.println("k = " + k++); // stampa 5 !!!!!

		long x = i + l;
		double media = (i + k) / 2.0; // per evitare di perdere
										// quello che segue la virgola

		int j = 2;

		double media2 = (i + k) / (double) j;

		int w = j + j++;
		System.out.println("w = " + w);

		int y;
		if (i > 13)
			y = 13;
		else
			y = 0;

		System.out.println(y);

		while (i > 13) {
			i--;
			System.out.println(i);
		}

		do {
			System.out.println(i);
			i--;
		} while (i > 0);

		switch (y) {
		case 0:
			System.out.println("ok");
			break;
		case 13:
			System.out.println("ko");
			break;
		default:
			System.out.println("unknown");
			break;
		}

		for (int g = 13; g <= 17; g++)
			System.out.println("g = " + g);
	}
}