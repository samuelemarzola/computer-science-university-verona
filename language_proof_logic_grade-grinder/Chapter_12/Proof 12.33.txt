Name: Samuele Marzola
Id: VR443652

P: ∀x ∀y SameShape(x,y)
G: ∀x Cube(x) ∨ ∀x Tet(x) ∨ ∀x Dodec(x)

Proof:
Per dimostrare che ∀x Cube(x) ∨ ∀x Tet(x) ∨ ∀x Dodec(x) prendo in considerazione il mondo di Tarski. La premessa afferma che preso un qualsiasi elemento x e un qualsiasi elemento y è vero che x e y hanno la stessa forma. 
Si conclude facilmente che ogni elemento x è o un cubo, o un tetraedro o un dodecaedro, considerando che il mondo di Tarski ammette solamente questi tre oggetti.
