/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Titolo: CipCiop
Autore: VR443652 - Samuele Marzola
Descrizione/Consegna:
Scrivere un programma C che stampa in sequenza i numeri da 1 a 1000 separati da una virgola, sostituendo con "CIP" i numeri che sono multipli di 3, con "CIOP" i numeri che sono multipli di 7 e con "CIPCIOP" i numeri che sono multipli sia di 3 che di 7. 
* ES: 1, 2, CIP, 4, 5, CIP, CIOP, 8, CIP, 10, 11, CIP, 13, CIOP, CIP, 16, 17, CIP, 19, 20, CIPCIOP, 22, 23, CIP, 25, 26, CIP, CIOP, 29, CIP, 31, 32, CIP, 34, CIOP, CIP, 37, 38, CIP, 40, 41, CIPCIOP, 43, 44 [ ... ] 

Data creazione: 12/12/2019
Ultima Modifica: 12/12/2019
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/

#include<stdio.h>
#define N 1000

int main() {

	int i;
	/* Leggenda variabili: 
		i -> variabile per il ciclo for
		*/
	
	// Stampa in sequenza i numeri da 1 a N
	for(i = 1; i <= N; i++) {
	
		// Se i è sia multiplo di 3 che di 7 scrive CIPCIOP
		if (i%3 == 0 && i%7 == 0) {
			printf("CIPCIOP, ");
		}
	
		// Se i è un multiplo di 3 stampa CIP
		else if (i%3 == 0) {
			printf("CIP, ");
		}
		
		// Se i è un multiplo di 7 scrive CIOP
		else if (i%7 == 0) {
			printf("CIOP, ");
		}
		
		// Altrimenti scrive il numero
		else {
			printf("%d, ", i);
		}
		
	}
	
	return 0;
}
