/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Titolo: Fibonacci
Autore: VR443652 - Samuele Marzola
Descrizione/Consegna:
Scrivere un programma C che memorizza in un array i primi 15 numeri di Fibonacci e li stampa a video.

Data creazione: 10/11/2019
Ultima Modifica: 13/11/2019
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/

#include<stdio.h>
#define N 15

int main() {
	int fib[N], i;
	
	// Riempe array con serie di Fibonacci
	for (i = 0; i <= N; i++) {
		if (i == 0 || i == 1) {
			fib[i] = 1;
		}
		else {
			fib[i] = fib[i - 2] + fib[i - 1];
		}
	}
	
	// Stampa array
	for (i = 0; i < N; i++) {
		printf("%d ", fib[i]);
	}
	
	printf("\n");
}
