/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Titolo: Array no duplicati
Autore: VR443652 - Samuele Marzola
Descrizione/Consegna:
Chiedere all’utente una sequenza di numeri interi che termina con l’inserimento dello 0 (e in ogni caso lunga al massimo 100 elementi). Creare un array che contenga tutti e soli valori distinti della sequenza (ovvero omettendo i duplicati). Visualizzare l’array e il numero di elementi unici
inseriti. 

Data creazione: 27/11/2019
Ultima Modifica: 27/11/2019
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/

#include<stdio.h>
#define MAX 100 // lunghezza massima array

int main() {
	int n[MAX], i, input, count = 0, inserire = 1;
	
	// Si ripete fino a quando non digita 0
	do {
	
		inserire = 1;
		
		printf("Elemento %d ", count);
		scanf("%d", &input);
		
		if (input != 0) {
		
			// Primo elemento digitato
			if (count == 0)  {
				n[count] = input;
				count++;
			}
				
			// Dal secondo elemento in poi
			else {
				// Se trova l'elemento digitato all'interno dell'array prepara la variabile inserire a 0
				for (i = 0; i <= count && inserire == 1; i++) {
					if (n[i] == input) {
						inserire = 0;
					}
				}
				
				// Se non è già presente lo inserisce
				if (inserire) {
					n[count] = input;
					count++;
				}
			}
		}
	}
	while (input != 0 && count <= MAX);
	
	// Stampa array
	for (i = 0; i < count; i++) {
		printf("%d ", n[i]);
	}
	
	// Stampa totale univoci
	printf("Totale univoci: %d \n", count);

	return 0;
}
