/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Titolo: 
Autore: VR443652 - Samuele Marzola
Descrizione/Consegna:


Data creazione: 10/12/2019
Ultima Modifica: 10/12/2019
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/

#include<stdio.h>
#define N 5

// Prototipo
void read(int m[N][N]);
void print(int m[N][N]);
int search(int m[N][N]);

void read(int m[N][N]) {
	int i, j;
	
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			scanf("%d", &m[i][j]);
		}
	}
}

void print(int m[N][N]) {
	int i, j;
	
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			printf("%d", m[i][j]);
		}
	}
}

int search(int m[N][N]) {
	int r, c;
	
	do {
		scanf("%d", &r);
	}
	while (r < 0 || r >= N);
	
	do {
		scanf("%d", &c);
	}
	while (c < 0 || c >= N);
	
	
	return(m[r][c]);
}

int main() {
	int m[N][N];
	int val;
	
	read(m);
	print(m);
	
	val = search(m);
	
	return 0;
}
