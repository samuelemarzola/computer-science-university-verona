/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Titolo: Pari / Dispari
Autore: VR443652 - Samuele Marzola
Descrizione/Consegna:
Dice se una serie di numeri sono pari o dispari

Data creazione: 03/12/2019
Ultima Modifica: 02/12/2019
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/

#include<stdio.h>

// Prototipo
void paridisp(int);

// Scrive se il numero come parametro è pari o dispari
void paridisp(int num) {

	if (n%2 == 0) {
		printf("Pari");
	}
	else {
		printf("Dispari");
	}

}

int main() {
	int i;
	
	for (i = 0; i <= 10; i++) {
		paridisp(i);
	}

	return 0;
}

