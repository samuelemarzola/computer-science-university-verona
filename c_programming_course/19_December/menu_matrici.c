/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Titolo: Menu
Autore: VR443652 - Samuele Marzola
Descrizione/Consegna:
Dice se una serie di numeri sono pari o dispari

Data creazione: 03/12/2019
Ultima Modifica: 03/12/2019
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/

#include<stdio.h>
#define N 10

int main() {

	int vet[N];
	int i, multipli = 0;
	int n,m;
	int scelta;
	int ins = 0;
	
	// Menu
	do {
		printf("\n 1 Inserisci ");
		printf("\n 2 Stampa ");
		printf("\n 3 Trova multipli ");
		printf("\n 4 Uscita \n");
	
		scanf("%d", &scelta);
	}
	while(scelta != 4);
	
	switch(scelta) {
		
		case 1: 
			for (i = 0; i < N; i++) {
				scanf("%d", &vet[i]);
			}	
			ins = 1;
			
			break;
			
		case 2:
			if(ins) {
				for (i = 0; i < N; i++) {
					printf("%3d", vet[i]);
				}
			}
			
			break;
			
		case 3:
			if(ins) {
				scanf("%d", &m);
				
				for (i = 0; p != 0; i++) {
					if (vet[i]%num == 0)
						multipli++;
						
					printf("%d", multipli);
				}
			}
			
			break;
			
		case 4: 
			break;
			
		default: 
			printf("Opzione non valida");
			
	}

	return 0;
}
