/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Title: Matrici
Author: VR443652 - Samuele Marzola
Description:
Scrivere un programma che date tre matrici A, B, C delle stesse dimensioni, e modifica C in modo che al termine dell'esecuzione si abbia C = A + B. 
Le matrici non sono necessariamente quadrate, ma devono avere tutte la stessa dimensione, in modo che sia possibile calcolarne la somma. 
Definire le dimensioni delle matrici con una costante simbolica.

Write a program that gives three matrices A, B, C of the same size, and modify C so that at the end of the execution we have C = A + B.
The matrices are not necessarily square, but they must all have the same size, so that it is possible to calculate their sum.
Define the dimensions of the matrices with a symbolic constant.

Creation Date: 12/12/2019
Last Edit Date: 12/12/2019
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/

#include<stdio.h>
#define DIM 2

int main() {

	int a[DIM][DIM];
	int b[DIM][DIM];
	int c[DIM][DIM];
	
	int i, j; // variables for cicle

	// Insert A
	for (i = 0; i < DIM; i++) {
		for (j = 0; j < DIM; j++) {
			printf("Matrix A - Value %d-%d: ", i, j); 
			scanf("%d", &a[i][j]);
		}
	}
	
	// Insert B
	for (i = 0; i < DIM; i++) {
		for (j = 0; j < DIM; j++) {
			printf("Matrix B - Value %d-%d: ", i, j); 
			scanf("%d", &b[i][j]);
		}
	}
	
	// Generate C (a + b)
	for (i = 0; i < DIM; i++) {
		for (j = 0; j < DIM; j++) {
			c[i][j] = a[i][j] + b[i][j];
			printf("%d ", c[i][j]);
		}
		printf("\n");
	}

	return 0;

}
