
/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Titolo: Massimo tra due numeri
Autore: VR443652 - Samuele Marzola
Descrizione/Consegna:
Dice se una serie di numeri sono pari o dispari

Data creazione: 03/12/2019
Ultima Modifica: 02/12/2019
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/

#include<stdio.h>

// Prototipo
int massimo(int, int);

// Restituisce il massimo tra due numeri
int massimo(int a, int b) {

	if (a > b) {
		return a;
	}
	else {
		return b;
	}

}


int main() {
	
	int a, b;
	int ris;
	
	scanf("%d%d", &a, &b);	
	
	ris = massimo(a, b);
	
	printf("Il massimo tra %d e %d e' %d", a, b, ris);
	
	return 0;
}
