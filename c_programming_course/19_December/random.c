/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Titolo: Random
Autore: VR443652 - Samuele Marzola
Descrizione/Consegna:
Genera numeri random

Data creazione: 10/12/2019
Ultima Modifica: 10/12/2019
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int main() {
	int casuale;
	
	srand(time(NULL));
	casuale = rand();
	
	/*
		between 30 e 70
		casuale = 30 + rand()%41 
		
		41 = 70 - 30 + 1
	*/
	
	printf("%d", casuale);
	printf("\n");
	
	return 0;
}
