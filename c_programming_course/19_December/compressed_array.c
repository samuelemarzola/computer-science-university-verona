/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Titolo: Array
Autore: VR443652 - Samuele Marzola
Descrizione/Consegna:
Scrivere un programma che riceve in input 20 valori interi. L'array è ordinato in modo crescente con valori ripetuti. 
Per ogni valore n, a partire dall'ultimo fino al primo, il programma visualizza n seguito dalle volte che compare nell'array. 
---

Data creazione: 03/12/2019
Ultima Modifica: 02/12/2019
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/

#include<stdio.h>
#define N 20 // lunghezza array

int main() {
	
	int i, times = 1;
	int vet[N];
	/* Leggenda variabili:
		i, j -> variabili per cicli
		times -> numero di volte che un numeor compare nell'array
		vet[] -> array contenente i valori inseriti dall'utente
	*/

	// Riempe l'array	
	for (i = 0; i < N; i++) {
		printf("Inserire valore %d: ", i + 1);
		scanf("%d", &vet[i]);
	}
	
	// Parte dall'ultimo elemento dell'array 
	for (i = N - 1; i >= 0; i--) {
	
		// Scorre all'indietro controllando se il valore precedente è uguale a se stesso
		while (vet[i] == vet[i - 1] && i >= 0) {
			times++;
			i--;
		}
		
		// Stampa il valore e il numero di volte che è ripetuto	
		printf("%d %d ", vet[i], times);
		
		// Rimposta la variabile times a 1
		times = 1;
	}
	
	printf("\n");
}
