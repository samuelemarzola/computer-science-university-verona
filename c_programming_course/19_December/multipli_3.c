/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Titolo: Conta multipli di 3
Autore: VR443652 - Samuele Marzola
Descrizione/Consegna:
Conta multipli di 3 in un array

Data creazione: 10/12/2019
Ultima Modifica: 10/12/2019
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/

#include<stdio.h>
#define N 10

// Prototipo
void read(int a[N]);
void print(int b[N]);
int count(int a[N]);

// Legge da tastiera valori da inserire in array
void read(int a[N]) {
	int i;
	
	for (i = 0; i < N; i++) {
		printf("Value: ");
		scanf("%d", &a[i]);	
	}
}

// Stampa valori contenuti in array
void print(int b[N]) {
	int i;
	
	for (i = 0; i < N; i++) {
		printf("%3d", b[i]);
	}
}

// Restituisce il numero di multipli di 3
int count(int a[N]) {
	int c = 0; 
	int i;
	
	for (i = 0; i < N; i++) {
		if (a[i]%3 == 0) {
			c++;
		}
	}

	return c;
}

int main() {
	int v[N], c;
	
	// Legge da tastiera e stampa array
	read(v);
	print(v);
	
	// Contiene il numero di multipli di 3 contenuti in array
	c = count(v);
	
	// Stampa numero di multipli di 3
	printf("%d", c);
	
	return 0;
}
