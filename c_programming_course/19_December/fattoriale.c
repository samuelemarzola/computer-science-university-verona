/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Titolo: Fattoriale
Autore: VR443652 - Samuele Marzola
Descrizione/Consegna:
---

Data creazione: 03/12/2019
Ultima Modifica: 02/12/2019
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/

#include<stdio.h>

// Prototipo
int fattoriale(int);

// Restituisce il fattoriale di un numero
int fattoriale(int num) {
	int i;
	int p = 1;
	
	for (i = 1; i <= num; i++) {
		p = p * i;
	}
	
	return(p);
}


int main() {
	int n, k;
	int f1, f2, f3;
	int r;
	
	scanf("%d%d", &n, &k);
	
	// Calcolo fattoriali
	f1 = fattoriale(n);
	f2 = fattoriale(k);
	f3 = fattoriale(n-k);
	
	r = f1/(f2+f3);
	
	printf("%d", r);
	
	return 0;
}
