#include<stdio.h>
#include<stdlib.h>

typedef struct list {
   int num;
   struct list * next;
} t_list;

t_list * inserisciintesta(t_list * lista, int numero){
   t_list * tmp;
   tmp = (t_list *)malloc(sizeof(t_list));

   if(tmp!=NULL){
      tmp -> num = numero;
      tmp -> next = lista;
      lista = tmp;
   }
   else {
      printf("Allocazione non riuscita");
   }
    
   return lista;
}

// Stampa la lista 
void printList(t_list * lista) {
   
   for (; lista != NULL; lista = lista -> next) {
      printf("%d\t", lista -> num);
   }

}

t_list * selectItems(t_list * list, int min, int max) {

   t_list * newList = NULL;

   int tmp = 0;

   while (list != NULL) {

      if (list -> num >= min && list -> num <= max) {
         newList = inserisciintesta(newList, list -> num);
      }

      list = list -> next;

   }

   return newList;
}

t_list * listaOrdinata(t_list * list) {

   t_list * R = list;
   int e;

   while (R != NULL) {
      e = R -> num;
      R = R -> next;

      if (e < R -> num) {
         list = inserisciintesta(list, e);

      }
      R = R -> next;
   
   }

   return list;

   /*t_list * tmp = (t_list *)malloc(sizeof(t_list));
   tmp -> num = list -> num;

   while (list != NULL) {

      if (tmp -> num > list -> next -> num && list -> next != NULL) {
         list -> next -> num = tmp -> num; 
         tmp -> num = list -> next -> num; 
         list -> num = tmp -> num;
      }

      list = list -> next;

   }

   return list;*/

}

int main () {

   t_list * list = NULL; 
   t_list * newList = NULL;
   
   int min, max;

   list = inserisciintesta(list, 12);
   list = inserisciintesta(list, 1);
   list = inserisciintesta(list, 15);
   list = inserisciintesta(list, 99);
   list = inserisciintesta(list, 36);

   // lista non ordinata
   printf("Non ordinata: ");
   printList(list);

   printf("\n");

   list = listaOrdinata(list);

   // lista ordinata 
   printf("Lista ordinata: ");
   printList(list);

   printf("\n");

    printf("----------------------\n");

   newList = selectItems(list, 2, 50);

   printList(newList);

   printf("\n");

   return 0;
}  