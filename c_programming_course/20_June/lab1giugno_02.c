/*
Sia data una lista contenente almeno due elementi ed i cui nodi sono definiti tramite la seguente struttura C: 
struct nodo {
    int valore1; 
    int valore 2;
    struct nodo *next; 
};
Si scriva una funzione C che conta quanti nodi della lista hanno i due valori che sono primi tra di loro. 
Scrivere sia la versione iterativa che ricorsiva della funzione.
Scrivere poi la funzione ricorsiva che stampa la lista. */

#include <stdio.h>
#include <stdlib.h>

struct nodo {
    int valore1;
    int valore2;
    struct nodo * next;
};

typedef struct nodo t_nodo;

int checkDivisori(int);

t_nodo * inserisciTesta (t_nodo * lista, int val1, int val2) {

    t_nodo * tmp;

    tmp = (t_nodo *)malloc(sizeof(lista));

    if (tmp) {
        
        tmp -> valore1 = val1;
        tmp -> valore2 = val2; 
        tmp -> next = lista;
        lista = tmp;
    }
    else {
        printf("Memoria esaurita\n");
    }

    return lista;
}

int countPrimi(t_nodo * lista) {

    int count = 0;
    int flag = 1;
    int div = 1;

    for (; lista != NULL; lista = lista -> next) {

        flag = 1;
        
            
        while(count == 0 && div <= lista -> valore2 / 2) {
            if(lista -> valore1 % div == 0 && lista -> valore2 % div == 0) {
                flag = 0;
            }	
            div++;
        }

        if (flag == 1) {
            count++;
        }

    } 

    return count;

}

void stampaLista(t_nodo * lista) {

    if (lista) {
        printf("%d\t%d\n", lista -> valore1, lista -> valore2);
        stampaLista(lista -> next);
    }

}

int main() {

    int primi;
    t_nodo * lista = NULL;

    lista = inserisciTesta(lista, 4, 8);
    lista = inserisciTesta(lista, 8, 9);
    lista = inserisciTesta(lista, 8, 100);
    lista = inserisciTesta(lista, 12, 25);
    lista = inserisciTesta(lista, 65, 88);
    lista = inserisciTesta(lista, 1, 2);
    lista = inserisciTesta(lista, 9, 25);

    stampaLista(lista);
    
    primi = countPrimi(lista);

    printf("Totale primi: %d", primi);

}