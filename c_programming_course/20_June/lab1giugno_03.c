/*
Scrivere un programma che apre un file di testo il cui nome è "TEST.txt" contenente numeri interi. 
Per ciascun numero letto, il programma stabilisce se è il doppio del suo precedente; in tal caso stampa a video la coppia di numeri.
NOTA: il numero di valori contenuti nel file non è noto (può anche essere vuoto o contenere un solo elemento!). 
Esempio: Contenuto del file: 1 2 5 2 4 8 5 4 5. Il programma stamperà 1 2 2 4 4 8. 
*/

#include <stdio.h>

int main() {

    FILE * file;

    int prev, value;

    file = fopen("test_03.txt", "r");

    if (file) {
        while (!feof(file)) {
            fscanf(file, "%d", &value);

            if (prev == -1) {
                prev = value;
            }
            else {
                if (value == (prev * 2)) {
                    printf("%d\t%d\t", prev, value);
                }
                prev = value;
            }
        }

        fclose(file);
    }
    else {
        printf("Impossibile aprire il file!\n");
    }

    return 0;
}
