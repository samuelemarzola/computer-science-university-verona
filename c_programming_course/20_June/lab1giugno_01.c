/*
Sia data una lista non vuota ed i cui nodi sono definiti tramite la seguente struttura C: 
struct nodo { 
    int valore; 
    struct nodo *next; 
}; 
Si scriva una funzione C che ricevendo in ingresso un puntatore alla lista modifichi la stessa, memorizzando nell’ultimo nodo il numero di valori dispari presenti nella lista e nel primo nodo il numero di valori pari presenti. Ad esempio, una lista contenente la sequenza di interi 4 6 2 3 9 verrà modificata dalla funzione nella lista 3 4 6 2 3 9 2. 
*/

#include<stdlib.h>
#include<stdio.h>

struct nodo { 
    int valore; 
    struct nodo *next; 
}; 

typedef struct nodo t_nodo;

// Definizione prototipo
t_nodo* pariDispari(t_nodo *);
t_nodo* insertHead(t_nodo *, int);
t_nodo* insertTile(t_nodo *, int);
void print(t_nodo *);

t_nodo* pariDispari(t_nodo * lista) {

    t_nodo * head;

    int pari = 0, dispari = 0;

    for (head = lista; head != NULL; head = head -> next) {

        // pari
        if ((head -> valore) % 2 == 0) {
            pari++;
        }
        else { // dispari
            dispari++;
        }

    }

    // mette al primo posto il totale dei pari
    lista = insertHead(lista, pari);

    // mette in coda il numero di dispari
    lista = insertTile(lista, dispari);

    return lista;
}

t_nodo* insertTile(t_nodo * lista, int value) {

    t_nodo * tmp;
    t_nodo * prec;

    tmp = (t_nodo *)malloc(sizeof(t_nodo));

    if (tmp != NULL) {

        tmp -> valore = value;
        tmp -> next = NULL;

        if (lista == NULL) {
            lista = tmp;
        }
        else { // inseirisci alla fine
            for(prec=lista;prec->next!=NULL;prec=prec->next);
            prec->next = tmp;
        }

        lista -> next = tmp;

    }

    return lista;
}

t_nodo* insertHead(t_nodo * lista, int value) {

    t_nodo * tmp;
    tmp = (t_nodo *)malloc(sizeof(t_nodo));

    if (tmp != NULL) {
        tmp -> valore = value;
        tmp -> next = lista;
        lista = tmp;
    }
    else {
        printf("Allocazione non riuscita!\n");
    }
    
    return lista;
}

void print(struct nodo * lista) {
    while (lista != NULL) {
        printf("%d\t", lista -> valore);
        lista = lista -> next;
    }

    printf("\n");
}

int main() {

    t_nodo * lista = NULL;

    // inserimento valori nella lista
    lista = insertHead(lista, 9);
    lista = insertHead(lista, 3);
    lista = insertHead(lista, 2);
    lista = insertHead(lista, 6);
    lista = insertHead(lista, 4);

    print(lista);

    lista = pariDispari(lista);

    print(lista);

    return 0;
}