/*

Scrivere un programma che apre un file di testo il
cui nome è "TEST.txt" contenente numeri interi.
Per ciascun numero letto, il programma stabilisce
se è il doppio del suo precedente; in tal caso stampa
a video la coppia di numeri. NOTA: il numero di
valori contenuti nel file non è noto (può anche
essere vuoto o contenere un solo elemento!).
Esempio:
Contenuto del file: 1 2 5 2 4 8 5 4 5
Il programma stamperà
12
24
48 

*/

#include <stdio.h>


int main() {

   FILE * file = fopen("test.txt", "r");

   int numero; // contiere i numeri inseirti nel file 
   int prev = 0; // contiene il precedente

   // se il file non esiste
   if (file == NULL) {
      printf("Errore");
   }
   else {

      // fintantochè non raggiunge la fine del file 
      while (!feof(file)) {
        
         // legge il numero dal file
         fscanf(file, "%d\n", &numero);

         if (numero == (prev * 2) && prev != 0) {
            printf("%d-%d\n", prev, numero);
         }

         prev = numero;
         
      }

   }

   return 0;
}