/* – – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Title: sequenza
Author: VR443652 - Samuele Marzola

Scrivere una funzione int sequenza(int a[], int dim) che prende in ingresso un array di interi di massimo 20 valori ORDINATI anche con ripetizioni (si assume che l’input sia corretto, non serve verificare) e la sua dimensione effettiva e restituisce il valore che si presenta il maggior numero di volte (o il primo di tali valori). 
Ad esempio, se la funzione riceve in ingresso l’array a[]={2,2,2,3,3,3,3,4,5,5,5,5,9} restituirà 3 

Creation Date: 02/03/2020 - Last Edit Date: 02/03/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – */

#include<stdio.h>
#define MAX 20

// Prototipo
int sequenza(int a[], int dim);

// Ritorna il valore più frequente
int sequenza(int a[], int dim) {

    int i; // variabile per ciclo
    int count = 0; // contatore
    int max = 0, freq; // max - quante volte si presenta, freq - valore più frequente

    // Ricerca valore più frequente
    for (i = 0; i < dim - 1; i++) {
        
        while (a[i] == a[i+1]) {
            count++;
            i++;
        }

        if (count > max) {
            max = count;
            freq = a[i];
        }

        count = 0;

    }

    return freq;

}

int main() {

    int array[MAX];
    int count = 0;
    int value, ris;

    printf("--- -1 per terminare --- \n");

    // Inserimento valori
    do {
        printf(">>> ");
        scanf("%d", &value);
        array[count] = value;
        count++;
    }
    while (count <= MAX && value != -1);

    // Calcolo valore più frequente
    ris = sequenza(array, count);

    // Stampa il valore più frequente
    printf("%d\n", ris);

    return 0;
}