/* – – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Title: numeri_primi
Author: VR443652 - Samuele Marzola

Scrivere un programma che riceve una sequenza di interi a priori illimitata (e che termina con l’inserimento del valore 0). Per ogni intero letto il sottoprogramma stampa tutti i valori primi strettamente inferiori del numero.

Creation Date: 02/03/2020 - Last Edit Date: 02/03/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – */

#include<stdio.h>

// Prototipo
void numeriprimi(int val);

// Stampa i numeri primi strettamente minori di val
void numeriprimi(int val) {

    int i, j; // variabili per ciclo
    int count = 0;

    // Ciclo che stampa i numeri primi strettamente minori di val
    for (i = 0; i < val; i++) { 

        // Ciclo che permette di calcolare i numeri primi
        for (j = 1; j <= i; j++) {

            // Se ha solo 2 divisori (1 e se stesso) è primo
            if (i%j == 0) {
                count++;
            }
        }

        // Se ha solo due divisori, stampa a video il numero
        if (count == 2) {
            printf("%d ", i);
        }

        // Rimette variabile contatore a zero
        count = 0;
    }

    printf("\n");

}

int main() {

    int val;

    printf("--- Termina con 0 --- \n");

    do {
        printf("Valore: ");
        scanf("%d", &val);

        numeriprimi(val); 
    }
    while (val != 0);

    return 0;
}