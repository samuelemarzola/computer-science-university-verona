/*
Scrivere un programma che acquisisce una stringa s1 di massimo 50 caratteri e ne costruisce una nuova s2 copiando il contenuto di s1 senza le vocali. Infine il programma visualizza s2.
Esempio: s1="straniero" -> s2="strnr".
*/

#include<stdio.h>
#define MAX 50

int main(){
  int i,j;
  char str1[MAX+1],str2[MAX+1];
  gets(str1);
  
  for(i=0,j=0;str1[i]!='\0';i++){
    if(str1[i]!='a' && str1[i]!='e' && str1[i]!='i' && 
       str1[i]!='o' && str1[i]!='u' && str1[i]!='A' && 
       str1[i]!='E' && str1[i]!='I' && str1[i]!='O' && 
       str1[i]!='U'){
      str2[j]=str1[i];
      j++;
    }
  }
  str2[j]='\0';
  printf("%s\n",str2);
}
