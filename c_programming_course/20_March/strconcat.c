//Scrivere un programma che acquisisce due stringhe di al più 30 caratteri e le concatena in un terzo array

#include<stdio.h>
#define N 30
 
int main()
{
  char s1[N+1], s2[N+1], s3[N*2+1];
  int i, n;
 
  scanf("%s", s1);
  scanf("%s", s2);
    
  for(i=0; s1[i]!='\0'; i++)
    s3[i]=s1[i];
    
  for(n=i,i=0; s2[i]!='\0';i++)
    s3[n+i]=s2[i];
    
  s3[n+i]='\0';
 
  printf("%s\n", s3);
}
