/* – – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Title: Cartina geografica
Author: VR443652 - Samuele Marzola

Dichiarare un tipo di dato per rappresentare una località su una cartina in termini di longitudine e latitudine (due interi) ed un nome (una stringa di al max 30 caratteri). Scrivere un programma che acquisisce i dati di dieci località ed individua la coppia di punti a distanza massima visualizzandone i nomi. Nel caso ci siano più punti con la stessa distanza massima stampare soltanto la prima coppia individuata. Per il calcolo della radice quadrata utilizzare la funzione sqrt() presente nella libreria math.h. La funzione riceve come parametro un numero in virgola mobile e ne restituisce la radice quadrata, anch'essa un numero in virgola mobile. Esempio di utilizzo: ris = sqrt(dato); dove ris e dato sono due variabili float.
VARIANTE: Nel caso ci siano più punti con la stessa distanza massima stampare TUTTE le coppie individuate.

Creation Date: 02/03/2020 - Last Edit Date: 02/03/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – */

#include<stdio.h>
#include<math.h>
#define L 31
#define MAX 10

int clear_input_buffer(void) {
    int ch;
    while (((ch = getchar()) != EOF) && (ch != '\n')) /* void */;
    return ch;
}

int main() {

    typedef struct {
        int longitudine;
        int latitudine;
        char nome[L];
    } local;

    local locali[MAX];   
    float distanza; 
    float distanza_max = 0;
    int i, j;
    int arg;
    int a, b; // indici dei due punti più distanti 

    // Get values
    for (i = 0; i < MAX; i++) {

        printf("Longitudine: ");
        scanf("%d", &locali[i].longitudine);
        clear_input_buffer();

        printf("Latitudine: ");
        scanf("%d", &locali[i].latitudine);
        clear_input_buffer();

        printf("Nome: ");
        fgets(locali[i].nome, L, stdin);
    }

    for (i = 0; i < MAX; i++) {
        for (j = i + 1; j < MAX; j++) {
            arg = (locali[j].longitudine - locali[i].longitudine) * (locali[j].longitudine - locali[i].longitudine) + (locali[j].latitudine - locali[i].latitudine) * (locali[j].latitudine - locali[i].latitudine);
            distanza = sqrt(arg);

            if (distanza > distanza_max) {
                distanza_max = distanza;
                a = i; 
                b = j;
            }
        }
    }

    printf("\nCOPIA DISTANZA MASSIMA\n\n");

    printf("%d - %d\n", locali[a].latitudine, locali[a].longitudine);
    puts(locali[a].nome);
    
    printf("%d - %d\n", locali[b].latitudine, locali[b].longitudine);
    puts(locali[b].nome);

    return 0;
}