/* – – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Title: Exponentiation
Author: VR443652 - Samuele Marzola
Scrivere un sottoprogramma che riceve due numeri interi positivi e calcola l'elevamento a potenza del primo rispetto al secondo, restituendo il risultato.
Write a subprogram that receives two positive integers and calculates the exponentiation of the first with respect to the second, returning the result.
Creation Date: 01/03/2020 - Last Edit Date: 01/03/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – */

#include<stdio.h>
#include<math.h>

// Return a raised to b
int exponentiation(int a, int b) {
    return pow(a, b);
}

int main() {

    int a, b;
    int pow; 

    // Get a
    printf("Value a: ");
    scanf("%d", &a);

    // Get b
    printf("Value b: ");
    scanf("%d", &b);

    // Calculate a raised to b
    pow = exponentiation(a, b);

    // Print the result
    printf("Result >> %d ", pow);
    printf("\n");

    return 0;
}