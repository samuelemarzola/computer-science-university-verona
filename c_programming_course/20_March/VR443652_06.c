/* – – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Title: Valore Massimo
Author: VR443652 - Samuele Marzola

Scrivere un sottoprogramma C che riceve come parametro una matrice quadrata 4x4 di numeri interi positivi e un intero m. 
Il sottoprogramma calcola e restituisce al chiamante il massimo valore <= m contenuto nella matrice se esiste; se tale valore non esiste verrà restituito il valore -1.

Creation Date: 23/03/2020 - Last Edit Date: 23/03/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – */

#include<stdio.h>
#define N 4

// Definizione prototipo
int readInt (int vmin);
int massimo(int mat[N][N], int m);

/* 
Acquisice in input un valore e controlla che il valore numerico intero inserito da tastiera dall'utente sia positivo. 
Restituisce il valore inserito dall'utente 
*/ 
int readInt (int vmin) {
   int numInt; // numInt = numero in input
				
    // Permette di rinserire il valore
    do {
        scanf("%d", &numInt);

        // Se l'utente ha sbagliato, stampa a schermo una stringa
        if (numInt < vmin) {
            printf("\tAttenzione! Valore non consentito.\n Riprovare: ");
        }
   }
   while (numInt < vmin);

   return numInt;
}

/* Calcola e restituisce al chiamante il massimo valore <= m contenuto nella matrice
Se non esiste restituisce -1 */
int massimo(int mat[N][N], int m) {
    
    int i, j; // variabili per cicli for
    int max = -1;

    // Ciclo for che scorre le righe della matrice
    for (i = 0; i < N; i++) {
        // Ciclo for che scorre le colonne della matrice
        for (j = 0; j < N; j++) {

            // Se il valore alla posizione i-j è minore del valore m passato come parametro ed è maggiore del valore salvato in max salva in max il valore alla posizione i-j. 
            if (mat[i][j] < m && mat[i][j] > max) {
                max = mat[i][j];
            }
            
        }
    }

    return max;

}

int main() {

    int mat[N][N]; // matrice per valori inseriti da tastiera
    int i, j; // variabili per cicli for
    int m; // valore massimo inserito da tastiera
    int max; // contiene il valore massimo <= m

    // Ciclo for che scorre le righe della matrice
    for (i = 0; i < N; i++) {
        // Ciclo for che scorre le colonne della matrice
        for (j = 0; j < N; j++) {
            printf("Valore %d - %d: ", i, j);
            mat[i][j] = readInt(0);
        } 
    }

    // Richiesta inserimento valore massimo
    printf("Valore massimo m: ");
    m = readInt(0);

    // Calcolo del valore massimo <= m
    max = massimo(mat, m);

    // Stampa del valore massimo <= m
    printf("Valore massimo <= m nella matrice: %d \n", max);

    return 0;
}