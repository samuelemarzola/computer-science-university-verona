/* – – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Title: Frazioni
Author: VR443652 - Samuele Marzola

Scrivere un programma che acquisisce due frazione ed esegue la somma. 
Il programma deve semplificare il risultato riportando l'eventuale segno meno nel numeratore. 

Creation Date: 14/03/2020 - Last Edit Date: 14/03/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – */

#include<stdio.h>

int main() {

    typedef struct  {
        int n;
        int d;
    } fraction_t;

    fraction_t a, b, sum; 
    int i, segno;

    // Frazione 1
    // Input numeratore
    scanf("%d", &a.n);

    // input denominatore
    do {
        scanf("%d", &a.d);
    }
    while (a.d == 0);

    // Frazione 2

    // Input numeratore
    scanf("%d", &b.n);

    // Input denominatore
    do {
        scanf("%d", &b.d);
    }
    while (b.d == 0);

    // somma tra due frazioni
    sum.d = a.d * b.d;
    sum.n = a.n * b.d + b.n * a.d;

    segno = 1;

    // Cambi di segno
    if (sum.n < 0) {
        sum.n =- sum.n;
        segno =- segno;
    }

    if (sum.d < 0) {
        sum.d =- sum.d;
        segno =- segno;
    }

    if (sum.n < sum.d) {
        i = sum.n;
    }
    else { 
        i = sum.d;
    }

    while (sum.n % i != 0 || sum.d % i != 0) {
        i--;
    }

    sum.n = sum.n / i * segno;
    sum.d = sum.d / i;

    printf("Somma: %d %d \n", sum.n, sum.d);

    return 0;
}