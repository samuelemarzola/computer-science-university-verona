/* – – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Title: Intervallo di tempo
Author: VR443652 - Samuele Marzola

Definire un tipo di dato per memorizzare una durata temporale in termini di numero di giorni, ore, minuti, secondi.
Scrivere un programma che riceva da tastiera i dati sulla durata di una serie di intervalli massimo 31. 
Ciascun intervallo è descritto in termini di ore, minuti e secondi. 
Il programma deve assicurarsi che ciascun dato sia valido.
Successivamente il programma calcola la somma di tutti gli intervalli.

Creation Date: 14/03/2020 - Last Edit Date: 14/03/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – */

#include<stdio.h>
#define MAX 31
#define ORE 24
#define MINUTI 60
#define SECONDI 60

int main() {

    typedef struct {
        int giorni;
        int ore;
        int minuti;
        int secondi;
    } intervallo_t;

    intervallo_t intervalli[MAX], somma;
    int size; // numero intervalli
    int i;  // variabile per i cicli

    // Quanti intervalli?
    do {
        scanf("%d", &size);
    }
    while (size <= 0 || size >= 31);

    // Legge ore, minuti, secondi
    for (i = 0; i < size; i++) {
        
        // Ore
        do {
            scanf("%d", &intervalli[i].ore);
        }
        while (intervalli[i].ore < 0 || intervalli[i].ore >= ORE);

        // Minuti
        do {
            scanf("%d", &intervalli[i].minuti);
        }
        while (intervalli[i].minuti < 0 || intervalli[i].minuti >= MINUTI);

        // Secondi
        do {
            scanf("%d", &intervalli[i].secondi);
        }
        while (intervalli[i].secondi < 0 || intervalli[i].secondi >= SECONDI);

        // azzerra il campo giorni
        intervalli[i].giorni = 0;

    }

    // Calcolo somma intervalli
    // azzerro prima tutti i campi
    somma.giorni = 0;
    somma.ore = 0;
    somma.minuti = 0;
    somma.secondi = 0;

    // Incremento ogni campo
    for (i = 0; i < size; i++) {
        somma.ore += intervalli[i].ore;
        somma.minuti += intervalli[i].minuti;
        somma.secondi += intervalli[i].secondi;        
    }

    // calcolo overflow ore, minuti, secondi
    somma.minuti += somma.secondi / SECONDI;
    somma.secondi = somma.secondi % SECONDI;

    somma.ore += somma.minuti / MINUTI;
    somma.minuti = somma.minuti % SECONDI;

    somma.giorni += somma.ore / ORE;
    somma.ore = somma.ore % ORE;

    printf("Giorni: %d, Ore: %d, Minuti: %d, Secondi: %d \n", somma.giorni, somma.ore, somma.minuti, somma.secondi);

    return 0;
}
