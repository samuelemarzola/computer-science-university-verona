/* – – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Title: Square Root
Author: VR443652 - Samuele Marzola

Scrivere un sottoprogramma che stampa a video la radice ennesima intera di un numero intero positivo. Il sottoprogramma prende come parametri il numero e il grado della radice. Il sottoprogramma restituisce 1 se la radice intera è precisa, in alternativa 0. Scrivere un programma che utilizza tale sottoprogramma per calcolare la radice ennesima intera di un numero e di un grado chiesti all'utente, e ne visualizza il risultato.

Write a subprogram that prints the nth integer root of a positive integer. The subroutine takes the number and degree of the root as parameters. The subroutine returns 1 if the integer root is precise, alternatively 0. Write a program that uses this subroutine to calculate the nth integer root of a number and degree requested by the user, and displays the result.

Creation Date: 01/03/2020 - Last Edit Date: 01/03/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – */

#include<stdio.h>
#include<math.h>

// Return 1 if the root is precise, 0 otherwise
int root(int value, int grade) {

    float pow_result;
    
    pow_result = pow(value, (1.0/grade));

    printf(">> %.2f", pow_result);

    if (pow_result == (int)pow_result) {
        return 1;
    }
    else {
        return 0;
    }
}

int main() {

    int value, grade;
    int precision;

    printf("Insert value: ");
    scanf("%d", &value);

    printf("Insert grade: ");
    scanf("%d", &grade);

    precision = root(value, grade);

    //DEBUG
    printf("\n%d\n", precision);

    return 0;
}