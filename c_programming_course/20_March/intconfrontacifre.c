/* – – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Title: confrontacifre
Author: VR443652 - Samuele Marzola

Scrivere una funzione int confrontacifre(intnum1, intnum2) che prende in ingresso due interi e restituisce 1 se num1 ha più cifre di num2, 0 se i due numeri hanno lo stesso numero di cifre, 2 se num2 ha più cifre di num1. Ad esempio, se num1 vale 123 e num2 vale 5 la funzione restituirà 1. Scrivere anche il main per testare la funzione.

Creation Date: 02/03/2020 - Last Edit Date: 02/03/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – */

#include<stdio.h>

// Prototipo
int confrontacifre(int num1, int num2);

// Ritorna 1 se num1 ha più cifre di num2, 0 altrimenti.
int confrontacifre(int num1, int num2) {

    int i; // variabile per il ciclo
    int cifre1 = 0, cifre2 = 0;

    // Calcolo cifre num1
    while (num1) {
        num1 /= 10;
        cifre1++;
    }

    // Calcolo cifre num2
    while (num2) {
        num2 /= 10;
        cifre2++;
    }

    // se il primo numero ha più cifre del secondo ritorna 1, 0 altrimenti
    if (cifre1 > cifre2) {
        return 1;
    }
    else {
        return 0;
    }
}

int main() {

    int a, b, ris;

    // Richiesta valore 1
    printf("Valore 1: ");
    scanf("%d", &a);

    // Richiesta valore 2
    printf("Valore 2: ");
    scanf("%d", &b);

    // Contronto
    ris = confrontacifre(a, b);

    // Scrive la risposta
    if (ris) {
        printf("Il primo numero ha piu' cifre del secondo.\n");
    }
    else {
        printf("Il secondo numero ha più cifre del primo.\n");
    }

    return 0;
}