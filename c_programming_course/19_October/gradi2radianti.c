// 10.10.2019

#include<stdio.h>
#define PI 3.14

int main() {
	float angolo, rad;

	printf("Angolo: ");
	scanf("%f", &angolo);
	rad = (angolo*PI/180);
	printf("Radianti: %f", rad);
	printf("\n");
}
