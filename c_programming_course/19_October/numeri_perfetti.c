/*
Con n>0, stampo i perfetti. K è perfetto se la sommatoria dei divisori = K.

SAMUELE MARZOLA
22/10/2019
*/

#include<stdio.h>

void main() {
	int num, i, div, sum; 
	
	do {
		scanf("%d", &num);
	}	
	while (num <= 0);
	
	i = 1; 
	
	while (i<n) {
		
		sum = 0;
		dv = 1;
		
		while (div < i) {
			sum += div;
			div++;
		}
		
		if (sum == i) 
			printf("%d", i);
		
		i++;
	}
}
