/*
Scrivere un programma che ricevuto in ingresso un
numeri interi positivi a (se così non è, lo richiede),
visualizza un quadrato di lato a usando il carattere ‘X’
sulla diagonale principale e il carattere ’*’ altrove.

SAMUELE MARZOLA 
24.10.2019
*/

#include<stdio.h>

void main() {
	int l, i, k;
	
	do {
		printf ("Inserire lato: ");
		scanf("%d", &l);
		
		if (l<0) {
			printf("Inserire un numero intero positivo");
		}
	}
	while (l<0);
	
	i=0;
	while (i < l) {
		
		k = 0;
		while (k < l) {
		
			if (k-i==0)
				printf("X");
			else 
				printf("*");
				
			k++;
		}
		
		printf("\n");
		
		i++;
		
	}
}
