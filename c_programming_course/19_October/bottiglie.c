// 10.10.10
// Samuele Marzola

#include<stdio.h>

int main() {
    float soldi;
    int bottiglie, resto = 0, prezzoBottiglia = 40, moneta1 = 20, moneta2 = 10, moneta3 = 5, moneta4 = 1;

    // ripeto input fino a quando ho un valore maggiore del prezzo della bottiglia
    do {
        printf("Digitare soldi a disposizione: ");
        scanf("%f", &soldi);

        soldi = soldi*100;

        if (soldi < prezzoBottiglia)
            printf("Inserire un valore maggiore o uguale a %d centesimi! \n", prezzoBottiglia);
    }
    while (soldi < prezzoBottiglia);

    // Calcolo numero bottiglie
    bottiglie = soldi/prezzoBottiglia;
    printf("Puoi acquistare %d bottiglie \n", bottiglie);

    // Calcolo resto
    resto = soldi - bottiglie*prezzoBottiglia;

    if (resto == 1)
        printf("Resto totale %d centesimo \n", resto);
    else 
        printf("Resto totale %d centesimi \n", resto);
    
    printf("Resto monete da 20: %d \n", resto/moneta1);
    resto = resto - (moneta1 * (int)(resto/moneta1));

    printf("Resto monete da 10: %d \n", resto/moneta2);
    resto = resto - (moneta2 * (int)(resto/moneta2));

    printf("Resto monete da 5: %d \n", resto/moneta3);
    resto = resto - (moneta3 * (int)(resto/moneta3));
    
    printf("Resto monete da 1: %d \n", resto/moneta4);    
}