/*
Scrivere un programma che ricevuto in ingresso due
numeri interi positivi a e b (se così non è, li richiede),
visualizza un rettangolo di dimensione a*b usando il
carattere '*’.
*/

#include<stdio.h>

void main() {
	int a, b, i, k;
	
	do {
		printf ("Inserire primo numero: ");
		scanf("%d", &a);
		
		if (a<0) {
			printf("Inserire un numero intero positivo");
		}
	}
	while (a<0);
	
	do {
		printf("Inserire secondo numero: ");
		scanf("%d", &b);
		
		if (b<0) {
			printf("Inserire un numero intero positivo");
		}
	}
	while (b<0);
	
	i = 0;
	
	// Riga
	while (i<b) {
		
		k = 0;
		
		// Colonna
		while(k<a) {
			printf("*");
			k++;
		}
			
		printf("\n");
		
		i++;
		
	}
}
