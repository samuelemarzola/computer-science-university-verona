/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Titolo: Matrice di caratteri
Autore: VR443652 - Samuele Marzola
Descrizione/Consegna:
Scrivere un sottoprogramma che riceve come parametro una matrice quadrata 5x5 di caratteri (qualsiasi valore char).
Il sottoprogramma individua il carattere che compare più frequentemente e lo restituisce al chiamante (si ipotizzi che sia sempre unico).
Inoltre il sottoprogramma visualizza il contenuto della matrice, mostrando però uno spazio al posto dei caratteri uguali al carattere individuato.

Data creazione: 17/01/2020
Ultima Modifica: 18/01/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/
#include <stdio.h>
#define N 5
// N - Grandezza matrice righe/colonne

// Definizione prototipo
void print(char M[N][N]);
void substitute(char M[N][N], char c);
int frequency(char M[N][N], char charToCheck);
char mostFrequent(char M[N][N]);
void insert(char M[N][N]);

int main() {
	int i,j;
	char M[N][N];
	char freq;
	/* 	Leggenda Variabili: 
		i, j - variabili per ciclo for
		M - matrice contenente i valori inseriti da tastiera
		freq - carattere più frequente */

	// Inserimento di valore nella matrice
	insert(M);

	// freq contiene il carattere più frequente
	freq = mostFrequent(M);
	printf("Carattere più frequente: %c", freq);
	
	// sostituzione del carattere più frequente con uno spazio
	substitute(M, freq);

	printf("\n");

	// Stampa della matrice finale
	print(M);

	return 0;
}

// Stampa la matrice che riceve come parametro
void print(char M[N][N]) {
	int i, j;
	/* 	Leggenda Variabili: 
		i, j - variabili per ciclo for */	

	for (i=0; i<N; i++){
		for (j=0; j<N; j++){
			printf(" %c", M[i][j]);
		}
		printf("\n");
	}
}

// Sostituisce il carattere più frequente con il carattere spazio
void substitute(char M[N][N], char c) {
	int i, j;
	/* 	Leggenda Variabili: 
		i, j - variabili per ciclo for */	
	
	for (i=0; i<N; i++){
		for (j=0; j<N; j++){
			if (M[i][j] == c) {
				M[i][j]=' ';
			}
		}
	}
}

//sottoprogramma che restituisce valore maggiormente frequente
int frequency(char M[N][N], char charToCheck){
	int i,j, count = 0;
	/* 	Leggenda Variabili: 
		i, j - variabili per ciclo for
		count - numero di volte in cui è presente il carattere nella matrice */	
	
	for (i=0; i<N; i++){	
		for (j=0; j<N; j++){
			// Se valore della matrice è uguale alla variabile passata come parametro, incrementa contatore
			if (M[i][j] == charToCheck) {
				count++;
			}
		} 
	}

	return(count);
}

// Trova il carattere piu frequente nella matrice
char mostFrequent(char M[N][N]){
	char c;
	int i,j, count = 0, freq_max = 0;
	/* 	Leggenda Variabili: 
		f - carattere più frequente
		i, j - variabili per ciclo for
		count - numero di volte in cui è presente il carattere nella matrice
		freq_max - numero di frequenza massimo */

	for (i=0; i<N; i++) {
		for(j=0; j<N; j++){	

			// count contiene la frequenza del carattere nella matrice
			count = frequency(M,M[i][j]);
			
			// se la frequenza risulta maggiore del valore salvato in freq_max, aggiorna i valori delle rispettive variabili
			if(count >= freq_max){	
				freq_max = count;
				c=M[i][j];
			}
		} 
	}

	return(c);
}

// Permette di inserire i valore nella matrice
void insert(char M[N][N]) {
	int i,j;
	/* 	Leggenda Variabili: 
		i, j - variabili per ciclo for
	*/

	for (i=0; i<N; i++) {	
		for (j=0; j<N; j++){
			printf("Inserire valore: ");
			scanf("\n%c", &M[i][j]);
		}
	}
}