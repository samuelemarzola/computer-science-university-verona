/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Titolo: Matrice e funzione
Autore: VR443652 - Samuele Marzola
Descrizione/Consegna:
Si scriva un programma C che permetta all’utente di inizializzare una matrice di interi di dimensione NxN(con N dato) con numeri interi compresi tra 0 e 99 (estremi inclusi). Dopo aver stampato la matrice a video, il programma deve chiedere all’utente di inserire il valore di un intero vale deve poi contare e stampare a video le occorrenze di valdi ogni riga della matrice. Infine, il programma deve sostituire con uno 0 le occorrenze di val nelle sole righe della matrice con più di due occorrenzedi val

Data creazione: 9/01/2019
Ultima Modifica: 9/01/2019
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/
#include <stdio.h>
#define N 10

void insert(int mat[][]) {
	int i, j;
	
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			do {
				if (mat[i][j] < 0 && mat[i][j] > 99) {
					printf("Errore, inserire un numero compreso tra 0 e 99 \n");
				}
				printf("Valore %d %d: " i, j);
				scanf("%d", &mat[i][j]);
			}
			while (mat[i][j] < 0 || mat[i][j] > 99);
		}
	}
}

void print(int mat[][]) {
	int i;
	
	for (i = 0; i < N; i++) {
		for (i = 0; i < N; i++) {
			printf("%d ", mat[i][j]);
		}
		printf("\n");
	}
}

void count(int val, int mat[][]) {
	int i, j, count = 0;
	
	for (i = 0; i < N; i++) {
		for (j = 0; i < N; j++) [
			if (val[i][j] == val) {
				count++;
			}
		}
		printf("Riga %d: %d" i, count);
		count = 0;
	}
	
}

int main () {	
	int mat[N][N]; 
	int val;
	
	insert(mat);
	print(mat);

	printf("Inserire val: ");
	scanf("%d" &val);
	
	count(val, mat);
	
	return 0;
}
