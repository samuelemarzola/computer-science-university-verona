/* – – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Title: Dominate in array
Author: VR443652 - Samuele Marzola
Description: In un array bidimensionale di valori interi, si definisce dominante ogni elemento dell’array che è strettamente maggiore di tutti gli elementi dell’array bidimensionale che si trova in basso a destra rispetto all’elemento stesso (si veda la figura), non considerando però tutti gli elementi presenti nell’ultima colonna e nell’ultima riga. 
Si realizzi un sottoprogramma che ricevuto in ingresso un array bidimensionale e qualsiasi altro parametro ritenuto strettamente necessario calcoli e restituisca al chiamante il numero di elementi dominanti presenti.
Nel contesto di utilizzo del sottoprogramma, sono presenti le seguenti direttive/istruzioni riportate di seguito.
Creation Date: 19/02/2020 - Last Edit Date: 19/02/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – */

#include<stdio.h>
#define N 8
#define M 4

// Return the total number of dominate values
int dominate(int matrix[M][N]) {
    int i, j, k, l;
    int dominate = 1;
    int count = 0;

    for(i = 0; i < M - 1; i++) {
        for(j = 0; j < N - 1; j++) {

            for(k = i + 1; k < M; k++) {
                for(l = j + 1; l < N; l++) {

                    if (matrix[i][j] <= matrix[k][l]) {
                        dominate = 0;
                    }

                }
            }

            if (dominate) {
                count++;
            }

            dominate = 1;
        }
    }

    return count;
}

int main() {

    int matrix[M][N];
    int i, j;
    int n_dominate = 0; // will contains the total number of dominate values

    // Insert
    for (i = 0; i < M; i++) {
        for (j = 0; j < N; j++) {
            printf("Value %d - %d: ", i, j);
            scanf("%d", &matrix[i][j]);
        }
    }

    // Print
    for (i = 0; i < M; i++) {
        for (j = 0; j < N; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }

    n_dominate = dominate(matrix);

    printf("Number of dominate values: %d \n", n_dominate);

    return 0;
}