/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Titolo: Sequenza crescente
Autore: VR443652 - Samuele Marzola
Descrizione/Consegna:
Scrivere un programma C che legge da tastiera una sequenza di numeri (di lunghezza a priori indefinita e che termina con uno 0 che non fa parte della sequenza).
Stabile se si tratta di una sequenza crescente di numeri.

Data creazione: 12/02/2020
Ultima Modifica: 14/02/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/
#include<stdio.h>

int main() {

    int input; // valore inserito dall'utente da tastiera
    int temp; // variabiile per memorizzare il valore precedentemente inserito
    int incr = 1; // va a zero se non è crescente 

    printf("Inserire valore: ");
    scanf("%d", &input);

    temp = input;

    do {
        printf("Inserire valore: ");
        scanf("%d", &input);

        // Se il valore inserito è sempre maggiore del precedente allora è una sequenza crescente 
        if (input < temp && input != 0) {
            incr = 0;
        }

        temp = input;
    }
    while (input != 0);

    if (incr == 0) {
        printf("Non è crescente! ");
    }
    else {
        printf("E' crescente! ");
    }

    return 0;
}