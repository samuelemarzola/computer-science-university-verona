/* – – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Title: Search in array
Author: VR443652 - Samuele Marzola

Description: 
Si scriva una funzione in C, denominata cerca, che ricerchi la presenza di un elemento in un vettore di interi.
La funzione riceve in ingresso tre parametri:
    1.  un vettore di interi v[] nel quale ricercare il valore;
    2.  un un valore intero N che indica quanti elementi contiene il vettore;
    3.  il valore intero x che deve essere ricercato.
La funzione deve restituire un valore intero, ed in particolare:
    - se il valore x è presente nel vettore, allora la funzione restituisce l’indice della posizione alla quale si trova tale valore;
    - se il valore x è presente più volte, si restituisca l’indice della prima occorrenza;
    - se il valore x non è presente nel vettore, si restituisca -1.

Creation Date: 25/02/2020 - Last Edit Date: 25/02/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – */

#include<stdio.h>

// DEF prototype
int cerca(int v[], int N, int x);

// Return the index of the serched value if exist, otherwise returns -1
int cerca(int v[], int N, int x) {

    int i;
    int index = -1;

    for (i = 0; i < N; i++) {
        if (v[i] == x && index == -1) {
            index = i;
        }
    } 

    return index;
}

int main() {

    int N = 8;
    int v[] = {1, 2, 24, 10, 98, 32, 1, 8};
    int ret; 
    int to_search;

    printf("Value to search: ");
    scanf("%d", &to_search);

    ret = cerca(v, N, to_search);

    printf("%d", ret);
    printf("\n");

    return 0;
}