/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Title: Dividers
Author: VR443652 - Samuele Marzola
Description:
Sia dato un numero intero positivo N inserito da tastiera. 
Si scriva un programma in linguaggio C che calcoli i numeri interi che sono divisori di N. 
Dire inoltre se N è un numero primo.

A positive integer N entered from the keyboard is given.
Write a C language program that computes the integers that are divisors of N.
Also say if N is a prime number.

Creation Date: 14/02/2020
Last Edit Date: 14/02/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/

#include<stdio.h>

int main() {

    int num, i;
    int count;

    printf("Insert value: ");
    scanf("%d", &num);

    for (i = 1; i <= num; i++) {
        if (num % i == 0) {
            printf("%d is divider. \n", i);   
            count++;        
        }
    }

    if (count == 2) {
        printf("%d is a prime number. \n", num);
    }

    return 0;
}