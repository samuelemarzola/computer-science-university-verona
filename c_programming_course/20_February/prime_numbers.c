/* – – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Title:  Prime numbers
Author: VR443652 - Samuele Marzola
Description: Scrivere un programma C che legge da tastiera una sequenza di numeri (di lunghezza a priori
indefinita e che termina con uno 0 – che non fa parte della sequenza) e stampare quanti di
essi sono primi.
Creation Date: 19/02/2020 - Last Edit Date: 19/02/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – */
#include<stdio.h>

// Returs 1 if is prime, 0 otherwise 
int isprime(int value) {
    int i = 0;
    int isprime = 0;
    int count = 0;

    for (i = 1; i <= value; i++) {
        if (value%i == 0) {
            count++;
        }
    }

    if (count == 2) {
        return 1;
    }
    else {
        return 0;
    }
}

int main() {

    int input; // input
    int count = 0; // n of prime numbers

    do {
        printf("Value: ");
        scanf("%d", &input);

        if (isprime(input)) {
            count++;
        }
    }
    while (input != 0);

    printf("Number of prime vales: %d \n", count);

    return 0;
}