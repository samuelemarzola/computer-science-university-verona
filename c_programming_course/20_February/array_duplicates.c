/* – – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Title: Duplicates in array
Author: VR443652 - Samuele Marzola
Description: Compare each value in the array to see if there are repeating values ​​inside
Creation Date: 19/02/2020 - Last Edit Date: 19/02/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – */

#include<stdio.h>
#define L 8

// Check if there are duplicates inside the array 
int checkDuplicates(int array[]) {

    int i, j;
    int duplicates = 0;
    int cont = 0;

    for (i = 0; i < L; i++) {
        for (j = 0; j < L; j++) {
            if (array[j] == array[i]) {
               cont++;
            }
        }
        if (cont >= 2) {
            duplicates = 1;
        }

        cont = 0;
    }

    return duplicates; // 0 - no duplicates inside array, 1 otherwise 
}

int main() {
    int array[L] = {24, 10, 98, 60, 41, 24, 25, 1};
    int duplicates; // 0 - no duplicates inside array, 1 otherwise 

    duplicates = checkDuplicates(array);

    if (duplicates) {
        printf("There are duplicates inside the array \n");
    }
    else {
        printf("There aren't duplicates inside the array \n");
    }

    return 0;
}