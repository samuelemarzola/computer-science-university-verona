/* – – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Title: Fibonacci
Author: VR443652 - Samuele Marzola
Scrivere un programma C che stampa i primi 100 numeri di Fibonacci
Ricordiamo che la successione dei numeri di Fibonacci è definita come segue:
F(0) = 0
F(1) = 1
F(n) = F(n-1) + F(n-2)     per n > 1. 
Creation Date: 25/02/2020 - Last Edit Date: 25/02/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – */

#include<stdio.h>
#define N 100

int main() {

    int i;

    double a = 0;
    double b = 1;
    double temp = 0;

    printf("0 ");

    for (i = 1; i <= N; i++) {
        printf("%.0f ", a + b);
        temp = b;
        b = a;
        a = a + temp;
    }

    printf("\n");

    return 0;
}