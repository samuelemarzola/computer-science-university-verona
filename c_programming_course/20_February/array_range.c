/* – – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Title: Range in array
Author: VR443652 - Samuele Marzola
Description: Scrivere un sottoprogramma che riceve in ingresso un array di valori interi v e qualsiasi altro
parametro ritenuto necessario, ed altri due valori interi da e a. Il sottoprogramma verifica se
nell’array sono presenti tutti e soli i valori inclusi nell’intervallo [da,a], senza ripetizioni. In
caso positivo il sottoprogramma restituisce 1, 0 in caso contrario.
Creation Date: 19/02/2020 - Last Edit Date: 19/02/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – */

#include<stdio.h>
#define N 8

// Return 1 if in the array there are all the element of the interval
int range_array(int v[], int da, int a) {

    int i;
    int check = 1;

    for (i = da; i < a; i++) {
        if(v[i] != (v[i+1] - 1)) {
            check = 0;
        }
    }

    return check;
}

int main() {

    int v[N] = {10, 18, 2, 3, 4, 5, 10, 24};

    printf("%d\n", range_array(v, 1, 5));
    printf("%d\n", range_array(v, 4, 5));

    return 0;
}