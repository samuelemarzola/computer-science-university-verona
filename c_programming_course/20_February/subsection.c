/* – – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Title: Sub-Section
Author: VR443652 - Samuele Marzola
Description: Realizzare un programma che prende in input una sequenza di caratteri ‘0’ e ‘1’ e conta la lunghezza della più lunga sotto-sequenza di ‘0’ di fila
Creation Date: 25/02/2020 - Last Edit Date: 27/02/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – */

#include<stdio.h>

int main() {

    int value; // insert by the user
    int max = 0;
    int count = 0;
    int prev = -1;

    printf("-1 to terminate \n");

    do {
        printf(">>> ");
        scanf("%d", &value);

        if(prev == -1) {
            prev = value;
        }

        if (value == 0) {
            count++;
        }
        else {
            if (count > max) {
                max = count;
            }
            count = 0;
        }

        prev = value;
    }
    while(value != -1);

    printf("%d", max);
    printf("\n");

    return 0;
}