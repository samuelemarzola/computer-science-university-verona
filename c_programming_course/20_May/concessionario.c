/* – – – – – – – – – – – – – – – – – – – – – – – – – – – – –
Title: Concessionario
Author: VR443652 - Samuele Marzola

Date le strutture dati che rappresentano un concessionario con il relativo gestore e le auto vendute:
Realizzare una funzione che riceve come parametro un array di tipo t_concessionario (e la sua dimensione). Per ogni concessionario la funzione stampa a video modello, targa e mese (in numero) delle auto immatricolate nel 2015. Il report mostrato dovrà avere il seguente formato:

Conc. 0, codice 12345: gestore Paolo Rossi
Immatricolazioni 2015:
* mese 9: Punto, MI80980
* mese 6: Marea, TO12567

Conc. 1, codice 23456: gestore Luca Bianchi
Immatricolazioni 2015:
* mese 2: Panda, VE85980
...

Infine scrivere uno stralcio di main() in cui va SOLO dichiarato un array di 10 elementi di tipo t_concessionario ed eseguita la chiamata alla funzione precedentemente definita.

Creation Date: 05/05/2020 - Last Edit Date: 05/05/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – */

#include<stdio.h>

#define N_CONCESSIONARI 10
#define MAX_STR 30
#define MAX_TRG 7
#define MAX_AUTO_CONCE 50

typedef struct{

  char modello[MAX_STR+1], targa[MAX_TRG+1];
  int meseImmatricolazione, annoImmatricolazione;

} t_auto;

typedef struct{

  char piva[MAX_STR+1];
  char nome[MAX_STR+1], cognome[MAX_STR+1];

} t_persona;

typedef struct{

  int codiceConcessionario;
  t_persona gestore;
  t_auto automobile[MAX_AUTO_CONCE];
  int nAuto; /* numero effettivo auto nel concessionario*/

} t_concessionario;

// Stampa i concessionari a video e le relative macchine immatricolate nel 2015
void print2015(t_concessionario concessionari[N_CONCESSIONARI], int lenght) {

    int i, j; // variabili per cicli
    int count = 0; // se resta a zero significa che nessuna auto del concessionario è immatricolata nel 2015
    int YearToCheck = 2015; // anno da controllare

    for (i = 0; i < lenght; i++) {

        printf("Conc. %d, codice %d: gestore %s %s\n", i+1, concessionari[i].codiceConcessionario, concessionari[i].gestore.nome, concessionari[i].gestore.cognome);
        printf("Immatricolazioni 2015:\n");

        for (j = 0; j < concessionari[i].nAuto; j++) {
            // Elenca solamente le auto immatricolate nel 2015
            if (concessionari[i].automobile[j].annoImmatricolazione == YearToCheck) {
                count++;
                printf("* mese: %d: %s, %s\n", concessionari[i].automobile[j].meseImmatricolazione, concessionari[i].automobile[j].modello, concessionari[i].automobile[j].targa);
            }
        }

        if (!count) {
            printf("Nessuna immatricolazione!\n");
        }

        count = 0;

        printf("\n");
    }

}

int main () {

    int lenght = 3; // numero effettivo di concessionari

    t_concessionario concessionari[N_CONCESSIONARI] = {
        {
            1001,
            {"IT6851614829", "Dario", "Matassa"},
            {
                {"Abarth 500", "RSLXUHU", 1, 2000},
                {"Alfa Romeo Giulietta", "Y2ZKN7B", 4, 2018},
                {"Renault Zoe", "S90SFI4", 7, 2015},
                {"Audi A3", "31GKC1K", 12, 2014},
                {"BMW i3", "9OOXET1", 1, 2017},
                {"BMW Z8", "0LUQJ4B", 1, 2015},
                {"Alfa Romeo Spider", "CRXCI9L", 1, 2015},
                {"Renault Capture", "GGWSK60", 9, 2014},
            },
            8,
        },
        {
            1002,
            {"IT4303576353", "Francesco", "Toneatti"},
            {
                {"Citroen C4", "P856LTT", 10, 2020},
                {"Wolkswagen Polo", "Z2RQ1JI", 5, 2015},
                {"Fiat 500", "PP6QL64", 9, 2015},
                {"Fiat Panda", "FBDA3KB", 10, 2015},
                {"Wolkswagen T-Cross", "I7J07JV", 12, 2008},
            },
            5,
        },
        {
            1003,
            {"IT6740446199", "Davide", "Franceschelli"},
            {
                {"DR City Cross", "VVB42TK", 12, 2015},
                {"Fiat Tipo", "6OVPUG6", 6, 2017},
            },
            2,
        }
    };

    print2015(concessionari, lenght);

    return 0;
}
