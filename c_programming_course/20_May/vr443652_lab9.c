/* – – – – – – – – – – – – – – – – – – – – – – – – – – – – –
Title: Magazzino ricambi auto
Author: VR443652 - Samuele Marzola

Creation Date: 07/06/2020 - Last Edit Date: 07/06/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*********  Variabili globali e strutture ********/

#define MAX 10 // massimo numero di pesate 

typedef struct measurement {
	int count; // progressivo pesata
	int id; // identificativo materiale
	char *object[32];
	float weight; // peso
	struct measurement *next;
} t_measurement;

/*********  Definizioni prototipo ********/

int readInt(int minVal, int maxVal);
t_measurement * pesa(t_measurement * weightList, int id);
void printList(t_measurement * weightList);
t_measurement * destroyList(t_measurement * weightList);
float randomFloat(float a, float b);   

/********* Funzioni ********/

float randomFloat(float a, float b) {
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = b - a;
    float r = random * diff;
    return a + r;
}

/*
Acquisice in input un valore e controlla che il valore numerico intero inserito da tastiera dall'utente sia compreso in un certo range. 
Restituisce il valore inserito dall'utente 
*/ 
int readInt(int minVal, int maxVal) {
   int intNum; // intNum: numero in input
	
   // Permette, in caso venga passato come parametro prima il maxVal e poi minVal, di invertire i due valori
	if (minVal > maxVal) {
		int tmp = maxVal; // mem = variabile per memorizzare il valore */
                
		maxVal = minVal;
		minVal = tmp;
	}
			
   // Permette di rinserire il valore
   do {
		scanf("%d", &intNum);
		
		// Se l'utente ha sbagliato, stampa a schermo una stringa
		if (intNum < minVal || intNum > maxVal) {
			printf("Attenzione! %d non è un valore consentito.\nIl numero deve essere compreso tra %d e %d\nRiprovare: \n", intNum, minVal, maxVal);
		}
	}
   while (intNum < minVal || intNum > maxVal);

   return intNum;
}

/*
Permette di pesare gli oggetti
Ritorna weightList
*/
t_measurement * pesa(t_measurement * weightList, int id) {

	srand(time(NULL));  

	static int count = 0;

	++count;

	if (count > MAX) {
		destroyList(weightList);
		exit(1);
	}
	else {

		// Istanzia una lista 
		t_measurement * tmp = (t_measurement *)malloc(sizeof(t_measurement));

		if(tmp == NULL) {
			printf("Memoria esaurita\n");
			exit(1);
		}

		t_measurement * prev;

		// Riempe la lista 
		tmp -> count = count;
		tmp -> id = id;
		
		switch (id) {
			case (1): {
				* tmp -> object = "GOMMA";
				tmp -> weight = randomFloat(9, 11);
				break;
			}

			case (2): {
				* tmp -> object = "PARAURTI";
				tmp -> weight = randomFloat(18, 22);
				break;
			}

			case (3): {
				* tmp -> object = "SPECCHIETTO";
				tmp -> weight = randomFloat(0.1, 1.5);
				break;
			}
		}

		// Quando non esiste un precedente perché si tratta del primo elemento della lista assegna tmp alla lista passata come parametro.
		if(weightList == NULL) {
			weightList = tmp;
		}
		else {
			for(prev = weightList; prev->next != NULL; prev = prev->next);
			prev->next = tmp;
		}

		// L'utente riceve a video le caratteristiche della pesata
		printf("Pesata %d: Questo/a %s pesa %3.2f kili\n", tmp -> count, *tmp -> object, tmp -> weight);
	}

	return weightList;
}

/*
Stampa a video la lista con le caratteristiche
*/
void printList(t_measurement * weightList) {

	if (weightList != NULL) {
		while(weightList != NULL) {  

			printf("Pesata %d -> %s, %3.2f kili\n\n", weightList -> count, *weightList -> object, weightList -> weight);
			weightList = weightList -> next;
		}
	}
	else {
		printf("Nessuna pesata effettuata \n");
	}
}

/*
La funzione distrugge la lista
*/
t_measurement * destroyList(t_measurement * weightList) {

	t_measurement * tmp;
  
	while(weightList != NULL) {

		tmp = weightList;
		weightList = weightList -> next;
        
		free(tmp);
	}

	return NULL;
}

int main() {

	int menu; // scelta menu
	t_measurement * weightList = NULL;
	
	// Menu
	do {
		printf("1 - Pesa \n");
		printf("2 - Vedi storico pesate \n");
		printf("0 - Esci \n");

		menu = readInt(0,2);
		
		switch (menu) {
			case (1): {

				int choice; // cosa vuole pesare

				printf("Cosa vuoi pesare? (1 -> Gomma, 2 -> Paraurti, 3 -> Specchietto) \n");
				choice = readInt(1,3);

				weightList = pesa(weightList, choice);

				break;
			}
			case (2): {

				printList(weightList);

				break;
			}
		}
	}
	while (menu != 0);

	destroyList(weightList);

   return 0;
}