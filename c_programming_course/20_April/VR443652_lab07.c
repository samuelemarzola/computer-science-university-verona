/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Titolo: Tris
Autore: VR443652 - Samuele Marzola
Descrizione/Consegna:
Scrivere un programma che implementi una versione semplificata di Tris a due giocatori. Ad ogni turno, il programma:
• chiede al giocatore 1 di inserire le coordinate della propria giocata; 
• inserisce il simbolo del giocatore 1 (’X’) nella posizione indicata;
• visualizza la griglia di gioco aggiornata;
• controlla se il giocatore 1 ha vinto;
• ripete le stesse operazioni per il giocatore 2 (simbolo ’O’).

Il programma prosegue finchè uno dei due giocatori non vince oppure fino a quando viene raggiunto il numero massimo di mosse disponibili. Si assuma che i due giocatori inseriscano sempre coordinate valide 

Data creazione: 16/04/2020
Ultima Modifica: 16/04/2020
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/

#include <stdio.h>
#define N 4

struct coordinate {
   int x;
   int y;
};

// Salva in memoria le coordinate x e y dei due giocatori
void inserisciCoordinate(struct coordinate *giocatore) {

   // Inserimento della coordinata x e check se il valore è valido:
   do {
      printf("Inserire coordinata X: ");
      scanf("%d", &giocatore->x);

      if (giocatore->x < 0 || giocatore->x > 2)
         printf("Coordinata non valida. \n");
   }
   while (giocatore->x < 0 || giocatore->x > 2);

   // Inserimento della coordinata y e check se il valore è valido:
   do {
      printf("Inserire coordinata Y: ");
      scanf("%d", &giocatore->y);

      if (giocatore->y < 0 || giocatore->y > 2)
         printf("Coordinata non valida. \n");
   }
   while (giocatore->y < 0 || giocatore->y > 2);

}

// Inserisce 'X' o 'O' a seconda del parametro passato nella posizione della griglia scelta dal giocatore
void aggiornaGriglia(char G[N][N], struct coordinate *giocatore, char charToInsert) {
   G[giocatore->x][giocatore->y] = charToInsert;
}

// Stampa a video la griglia del tris
void stampaGriglia(char G[N][N]) {
   
   int i, j;

   for (i = 0; i < N; i++) {
      for (j = 0; j < N; j++) {
         printf("%c", G[i][j]);
      }

      printf("\n");
   } 
}

// Controlla e restituisce 1 se l'utente ha vinto inserendo lo stesso carattere tre volte sulla stessa riga, 0 altrimenti
int checkRiga (char G[N][N], char toCheck) {
   int i, j;
   int count = 0;

   for (i = 0; i < N; i++) {
      for (j = 0; j < N; j++) {
         if (G[i][j] == toCheck) {
            count++;
         }
      }
   }

   return count == 3 ? 1 : 0; 
}

// Controlla e restituisce 1 se l'utente ha vinto inserendo lo stesso carattere tre volte sulla stessa colonna, 0 altrimenti
int checkColonna (char G[N][N], char toCheck) {
   int i, j;
   int count = 0;

   for (i = 0; i < N; i++) {
      for (j = 0; j < N; j++) {
         if (G[j][i] == toCheck) {
            count++;
         }
      }
   }

   return count == 3 ? 1 : 0; 
}

// Controlla e restituisce 1 se l'utente ha vinto inserendo lo stesso carattere tre volte nella diagonale da sinistra a destra, 0 altrimenti
int checkDiagonale1 (char G[N][N], char toCheck) {
   int i, j;
   int count = 0;

   for (i = 0; i < N; i++) {
      if (G[i][i] == toCheck) {
         count++;
      }
   }

   return count == 3 ? 1 : 0; 
}

// Controlla e restituisce 1 se l'utente ha vinto inserendo lo stesso carattere tre volte nella diagonale da destra a sinistra, 0 altrimenti
int checkdiagonale2 (char G[N][N], char toCheck) {
   int i, j;
   int count = 0;

   for (i = 0; i < N; i++) {
      for (j = N; j > 0; j--) {
         if (G[j][i] == toCheck) {
            count++;
         }
      }
   }
   
   return count == 3 ? 1 : 0; 
}

// Controlla e restituisce 1 se è stato fatto un tris,0 altrimenti
int checkVittoria (char G[N][N], char toCheck) {

   int vittoriaRiga = 0, vittoriaColonna = 0, vittoriaDiagonale1 = 0, vittoriaDiagonale2 = 0;

   vittoriaRiga = checkRiga(G, toCheck);
   vittoriaColonna = checkColonna(G, toCheck);
   vittoriaDiagonale1 = checkDiagonale1(G, toCheck);
   vittoriaDiagonale2 = checkdiagonale2(G, toCheck);
   
   return (vittoriaRiga || vittoriaColonna || vittoriaDiagonale1 || vittoriaDiagonale2) ? 1 : 0;
}

int main () {

   struct coordinate giocatoreA, giocatoreB; // conterranno le coordinate dei giocatori
   int mosse = 0; // contatore delle mosse disponibili -> blocca l'esecuzione quando esauriscono
   int vittoria = 0; // booleano 0/1 -> 1 in caso di vittoria
   char G[N][N] = { {'-','-','-'}, {'-','-','-'}, {'-','-','-'} }; // griglia di gioco
   char charA = 'X', charB = 'O'; // caratteri da inserire nella griglia

   // Interrompe l'esecuzione se uno dei due giocatori ha vinto o quando le mosse sono esaurite
   while (!vittoria && mosse < 9) {
     
      // TURNO GIOCATORE 1 
      printf("Turno giocatore 1 \n");
      mosse++; 
      inserisciCoordinate(&giocatoreA);
      aggiornaGriglia(G, &giocatoreA, charA);
      printf("\n");
      stampaGriglia(G);

      // Se il giocatore ha raggiunto il minimo numero di mosse per poter aggiudicarsi la vittoria, controlla se ha vinto
      if (mosse >= 5) {
         vittoria = checkVittoria(G, charA);

         if (vittoria) {
            printf("Vince il giocatore 1 \n");
         }
      }

      // Se il giocatore 1 non ha vinto, il turno passa al giocatore 2
      if (!vittoria && mosse < 9) {
         printf("Turno giocatore 2 \n");
         mosse++; 
         inserisciCoordinate(&giocatoreB);
         aggiornaGriglia(G, &giocatoreB, charB);
         stampaGriglia(G);

         // Se il giocatore ha raggiunto il minimo numero di mosse per poter aggiudicarsi la vittoria, controlla se ha vinto
         if (mosse >= 6) {
            vittoria = checkVittoria(G, charB);

            if (vittoria) {
               printf("Vince il giocatore 2 \n");
            }
         }
      }
   }

   return 0;
}