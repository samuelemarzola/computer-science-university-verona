#include<stdio.h>
#define N 10

int main() {
	int somma, i, voto;
	float media;
	
	i = 0;
	somma = 0;
	
	while(i<N) {
		scanf("%d", &voto);
		somma += voto;
		
		i++;
	} 
	
	media = (float)somma/N;
	
	printf("MEDIA: %1f", media);
	printf("\n");
}
