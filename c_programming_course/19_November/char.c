/*
Scrivere un programma che riceve in input un carattere minuscolo e visualizza il carattere successivo. Se l'utente inserisce z il programma scriverà a
Il programma termina quando l'utente inserisce un carattere che non sia una lettera minuscola.

SAMUELE MARZOLA
22/10/2019
*/

#include<stdio.h>

void main() {
	char c;
	
	scanf(" %c", &c);
	
	while(c >= 'a' && c <= 'z') {
		if (c == 'z') {
			c = 'a';
		}
		else {
			c += 1;
		}
		
		printf("%c \n", c);
		scanf(" %c", &c);
	}
}
