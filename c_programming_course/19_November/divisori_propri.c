/*
Scrivere un programma che chieda all'utente una sequenza indefinita di numeri positivi maggiori di zero, terminata da un numero negativo.
Il programma conta per ciascun valore della sequenza tutti i divisori propri. 
Es. 	1 5 4 3 2 8 9 0 
	1 0
	5 1
	4 2
	3 1
	2 1 
	8 3 
*/

#include<stdio.h>

void main() {
	int n, ndiv, div;
	
	scanf("%d", &n);
	
	while (n>0) {
	
		ndiv = 0;
	
		for (div = 1; div < n, div++) {
			if (n%div == 0) {
				ndiv++;
			}
		}
		printf("%d,%ndiv", ndiv);
		scanf("%d", &n);
	}
}
