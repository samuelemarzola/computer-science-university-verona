/*
Fattoriale di un numero
SAMUELE MARZOLA
*/

#include <stdio.h>

void main() {
	int num, fatt, i;
	
	scanf("%d", &num);
	
	while(num < 0)
		scanf("%d", &num);
		
	fatt = 1;
	i = 2;
	
	while (i<=num) {
		fatt = fatt * i;
		i++;
	}
	
	printf("%d", fatt);
}
