// Preso in input n, stampare i numeri interi da 0 a n.

#include<stdio.h>

int main() {

	int n, x;
	
	printf("  ");
	scanf("%d", &n);

	x = 0; // inizializzazione

	while(x <= n) {
		printf("%3d", x);
		x++;	// incremento variabile
	}
	
	printf("\n");

}
