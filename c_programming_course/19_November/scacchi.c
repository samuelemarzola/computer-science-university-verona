/*
SAMUELE MARZOLA 
19.10.2019

Su una scacchiera 8x8 sono posizionati due pezzi:il Re bianco e la Regina nera. Si scriva un
programma in linguaggio C che acquisisce le posizioni del Re e della Regina in termini di
coordinate (x,y) assumendo che la posizione (1,1) sia situata in basso a sinistra rispetto al giocatore.

Il programma controlla prima che le coordinate inserite siano valide; in particolare entrambi i
pezzi devono trovarsi all’interno della scacchiera ed inoltre non possono trovarsi nella stessa
posizione. In seguito il programma determina se la Regina è in posizione tale da poter mangiare il
Re e visualizza un apposito messaggio specificando anche in che direzione e per quante
posizioni deve muoversi per mangiare

*/

#include<stdio.h>

int main() {
	int regina_x, regina_y, re_x, re_y, delta_x, delta_y, diag;
	
	// Ripete la richiesta di input nel caso in cui re e regina si trovassero nella stessa posizione
	do {
	
		// Chiede X Regina Nera
		do {
			printf("Inserire coordinata X Regina Nera: ");
			scanf("%d", &regina_x);
			
			if (regina_x <1 || regina_x > 8) 
				printf("Coordinata non valida! Inserire un valore compreso tra 1 e 8\n");
		}
		while (regina_x < 1 || regina_x > 8);
		
		// Chiede Y Regina Nera 
		do {
			printf("Inserire coordinata Y Regina Nera: ");
			scanf("%d", &regina_y);	
			
			if (regina_y < 1 || regina_y > 8) 
				printf("Coordinata non valida! Inserire un valore compreso tra 1 e 8\n");
		}
		while (regina_y < 1 || regina_y > 8);

		// Chiede X Re Bianco
		do {
			printf("Inserire coordinata X Re Bianco: ");
			scanf("%d", &re_x);	
			
			if (re_x < 1 || re_x > 8) 
				printf("Coordinata non valida! Inserire un valore compreso tra 1 e 8\n");
		}
		while (re_x < 1 || re_x > 8);
		
		do {
			printf("Inserire coordinata Y Re Bianco: ");
			scanf("%d", &re_y);
		
			if (re_y < 1 || re_y > 8)
				printf("Coordinata non valida! Inserire un valore compreso tra 1 e 8\n");
		
		}
		while (re_y < 1 || re_y > 8);
	
		if (regina_x == re_x && regina_y == re_y)
			printf("Re Bianco e Regina Nera non possono trovarsi nella stessa posizione!\n");
	}	
	
	while (regina_x == re_x && regina_y == re_y);


	delta_x = regina_x-re_x;
	delta_y = regina_y-re_y;

	if (delta_x < 0) 
		delta_x *= -1;

	if (delta_y < 0)
		delta_y *= -1;

	diag = (delta_x + delta_y)/2;
		
		// Regina sulla stessa riga del re
		if (regina_y == re_y) {
			if (regina_x < re_x) 
				printf("La Regina deve muoversi di %d posizioni verso destra per poter mangiare il Re\n", delta_x);
			else
				printf("La Regina deve muoversi di %d posizioni verso sinistra per poter mangiare il Re\n", delta_x);
		}
		
		// Regina sulla stessa colonna del re
		else if (regina_x == re_x) {
			if (regina_y < re_y) 
				printf("La Regina deve muoversi di %d posizioni verso l'alto per poter mangiare il Re\n", delta_y);
			else 
				printf("La Regina deve muoversi di %d posizioni verso il basso per poter mangiare il Re\n", delta_y);
		}
		
		// Regina sulla stessa diagonale discendente del re 
		else if (regina_x + regina_y == re_x + re_y) {
			if (re_y > regina_y) 
				printf("La Regina deve muoversi di %d posizioni per la diagonale discendente verso l'alto per poter mangiare il Re\n", diag);
			else 
				printf("La Regina deve muoversi di %d posizioni per la diagonale discendente verso il basso per poter mangiare il Re\n", diag);
		}

		// Regina sulla stessa diagonale ascendente del re 
		else if (regina_x - regina_y == re_x - re_y) {
			if (re_y < regina_y) 
				printf("La Regina deve muoversi di %d posizioni per la diagonale ascendente verso il basso per poter mangiare il Re\n", diag);
			else 
				printf("La Regina deve muoversi di %d posizioni per la diagonale ascendente verso l'alto per poter mangiare il Re\n", diag);
		}	

		else 
			printf("La Regina non è in posizione tale da poter mangiare il Re.\n");		
}
