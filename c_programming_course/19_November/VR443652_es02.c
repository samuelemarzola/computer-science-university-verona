/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Titolo: Istogramma
Autore: VR443652 - Samuele Marzola
Descrizione/Consegna:
Scrivere un programma in C (matricola_es2.c) che chiede all'utente 5 valori interi non negativi e ne disegna l'istogramma a barre verticali come mostrato nel seguente esempio.
Se l'utente inserisce i valori: 1 4 5 2 1, il programma visualizzerà  il seguente output:
    *    
  * *    
  * *    
  * * *  
* * * * *
1 4 5 2 1 

Data creazione: 10/11/2019
Ultima Modifica: 13/11/2019
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/

#include<stdio.h>
#define N 5 // lunghezza array

int main() {

	int a[N], i, j, max = 0;
	/*	Leggenda variabili: 
		a -> array
		i, j -> variabili per i cicli for
		max -> valore massimo inserito dall'utente
	*/

	// inserisce nell'array i 5 valori inseriti dall'utente 
	for(i = 0; i < N; i++) {
		
		// ripete l'input all'utente nel caso venisse inserito un valore negativo
		do {
			printf("Inserire valore %d: ", i);
			scanf("%d", &a[i]);
			
			// se il valore inserito risultasse negativo stampa un messaggio d'errore
			if(a[i] < 0) {
				printf("Sono ammessi solo valori positivi! \n");
			}
			
			// aggiorna il valore della variabile max se l'utente inserisce un valore maggiore ai precedenti inseriti
			if(a[i] > max) {
				max = a[i];
			}
		}
		while(a[i] < 0);
		
	}
	
	// ciclo che stampa le righe dell'istogramma
	for(i = max; i > 0; i--) {
		
		// ciclo che stampa le colonne dell'istogramma
		for(j = 0; j < N; j++) {
		
			// se il valore contenuto nell'array è maggiore o uguale al counter stampa a video "*", altrimenti stampa uno spazio
			if (a[j] >= i) {
				printf("  *");
			}
			else {
				printf("   ");
			}
		
		}
		
		// nuova riga
		printf("\n");
		
	}
	
	// asse x istogramma: scorre e stampa tutti gli elementi dell'array
	for(i = 0; i < N; i++) {
		printf("%3d", a[i]);
	}
	
	printf("\n");

	return 0;
}
