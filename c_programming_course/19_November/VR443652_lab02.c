/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Titolo: Media Sequenza
Autore: VR443652 - Samuele Marzola
Descrizione/Consegna:
Scrivere un programma in C (matricola_lab02.c) per calcolare la media, il massimo e il minimo di una sequenza di voti inseriti dall'utente.
L'inserimento dei voti prosegue fino a quando l'utente non immette un valore non valido, cioè minore di 18 o maggiore di 30
(valore di «fine inserimento», che non fa parte della sequenza).
Quando viene immesso il valore di fine inserimento, vengono visualizzati media, massimo,
minimo e il numero di voti inseriti. Qualora il primo valore inserito fosse quello di fine inserimento,
dovrà essere visualizzato un messaggio di errore.

Data creazione: 28/10/2019
Ultima Modifica: 29/10/2019
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/

#include<stdio.h>

#define VOTOMIN 18
#define VOTOMAX 30

void main() {

	int	voto, 
			somma = 0, 
			max = VOTOMIN,
			min = VOTOMAX, 
			c = 0; // contatore voti inseriti
	float	media;
	
	// istruzioni
	printf("Valori accettati: >= 18 e <= 30. Valori fine inserimento: < 18 e > 30.\n");
	
	// ripete l'input fino a quando non viene inserito un valore di fine inserimento.
	do {
		printf("Inserire voto: "); 
		scanf("%d", &voto);
		
		// se si tratta di un voto, calcola somma, max, min e quantità
		if (voto >= VOTOMIN && voto <= VOTOMAX) {
			c++; // incremento contatore voti inseriti
			somma += voto;
			
			if (voto > max) 
				max = voto;
				
			if (voto < min) 
				min = voto;
		}
	}
	while (voto >= VOTOMIN && voto <= VOTOMAX);
	
	// se l'utente inserisce zero voti stampa errore, altrimenti stampa i risultati.
	if (!c)
		printf("Errore! Inserire almeno un voto.\n");
	
	else {
		media = (float)somma/c;
		printf("Media: %.2f", media);
		printf("\nMassimo: %d", max);
		printf("\nMinimo: %d", min);
		printf("\nTotale voti: %d\n", c);
	} 

}
