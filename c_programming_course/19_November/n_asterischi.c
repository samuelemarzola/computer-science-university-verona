/*
Chiesta sequenza di numeri stampo una riga di * tanti quanti il numero, inserire -1 per terminare
Samuele Marzola 
22.10.2019
*/

#include<stdio.h>

void main() {
	int n, i;
	
	n = 0;
	
	while(n != -1) {

		scanf("%d", &n);
		i = 1;
		
		if (n > 0) {
			while (i <= n) {
				printf("*");
				i++;
			}
		}
		
		printf("\n");
	}
}
