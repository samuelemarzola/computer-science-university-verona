/*
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
Titolo: Tavola pittagorica
Autore: VR443652 - Samuele Marzola
Descrizione/Consegna:
Scrivere un programma che stampa a video la tavola
pitagorica (un quadrato con le tabelline dei numeri da
1 a 10).

Data creazione: 24/10/2019
Ultima Modifica: 24/10/2019
– – – – – – – – – – – – – – – – – – – – – – – – – – – – – 
*/

#include<stdio.h>

void main() {

	int i, j;
	/* Leggenda variabili:
		i, j  -> variabili per cicli */
	
	// Ciclo for colonne
	for (i = 1; i <= 10; i++) {
	
		// Ciclo for righe
		for (j = 1; j <= 10; j++) {
			printf("%2d ", i * j);
		}
		
		printf("\n");
	}
}
